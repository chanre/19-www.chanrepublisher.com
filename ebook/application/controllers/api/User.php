<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('API_CRUD_model');
	}
	
	public function Login(){
		if(isset($_REQUEST['email']))
		{
			$email=$_REQUEST['email'];
			$password= isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
			$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '1';

			if($type == '2')
			{
				$where='email="'.$email.'"';
				$user_data = $this->API_CRUD_model->getById($where,'user');
				
				if($user_data){
					$user_data->image = base_url().'assets/images/user/'.$user_data->image;	
					if($user_data->id){
						$response=array('status'=>200,'message'=>'Successfully login','result'=>array($user_data));
					}else{
						$r=array();
						$response=array('status'=>400,'message'=>'Username and pasword is wrong','result'=>(object)array());
					}
				}
				else
				{
					// Facebook User Register 
					$email=$_REQUEST['email'];
					$fullname = isset($_REQUEST['fullname']) ? $_REQUEST['fullname'] : '';
					$mobile= isset($_REQUEST['mobile'])  ? $_REQUEST['mobile'] : '';

					$image = '';
					if(isset($_FILES['image']['name']))
					{
						$image = $this->API_CRUD_model->imageupload($_FILES['image'],'image', FCPATH . 'assets/images/user');
					}

					$data = array(
						'fullname' => $fullname, 
						'email' => $_REQUEST['email'],
						'type' => $type,
						'mobile' => $mobile,
						'image' => $image
					);
	
					$user_id=$this->API_CRUD_model->insert($data,'user');
					unset($data['password']);	
					$data['id']= $user_id;
					$data['image'] = base_url().'assets/images/user/'.$data['image'];	
					$response=array('status'=>200,'message'=>'Add user sucessfuly','result'=>array($data));
					echo json_encode($response);exit;
				}
			}else
			{
				$where='password="'.$password.'" and email="'.$email.'"';
				$user_data = $this->API_CRUD_model->getById($where,'user'); 
				
			if(isset($user_data->id)){
					$user_data->image = base_url().'assets/images/user/'.$user_data->image;	
					$response=array('status'=>200,'message'=>'Successfully login','result'=>array($user_data));
				}else{
					$r=array();
					$response=array('status'=>400,'message'=>'Username and pasword is wrong','result'=>(object)array());
				}
			}
		}else
		{
			$response=array('status'=>400,'message'=>'Please enter email and password ','result'=>(object)array());
		}
		echo json_encode($response);exit;
	}
	
	
	public function registration(){		
		if(isset($_REQUEST['fullname']) && isset($_REQUEST['email']) && isset($_REQUEST['password']) && isset($_REQUEST['mobile']))
		{
			$fullname= $_REQUEST['fullname'];
			$email= $_REQUEST['email'];
			$password= $_REQUEST['password'];
			$mobile= $_REQUEST['mobile'];

			$where='email="'.$email.'"';
			$emailresult = $this->API_CRUD_model->getById($where,'user'); 

			$where_mobile='mobile="'.$mobile.'"';
			$mobileresult = $this->API_CRUD_model->getById($where_mobile,'user'); 
	
				
			if(isset($emailresult->id)){
				$response=array('status'=>400,'message'=>'Email address already exists.','User_id'=>'');
				echo json_encode($response);exit;
			}

			if(isset($mobileresult->id)){
				$response=array('status'=>400,'message'=>'Mobile Number already exists.','User_id'=>'');
				echo json_encode($response);exit;
			}

			$data = array(
				'fullname'=>$fullname,
				'email'=>$email,
				'password'=>$password,
				'mobile'=>$mobile,
			);

			$user_id=$this->API_CRUD_model->insert($data,'user');
			$response=array('status'=>200,'message'=>'User registration sucessfuly','id'=>(string)$user_id);
			echo json_encode($response);exit;
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter all field','id'=>"");
			echo json_encode($response);exit;
		}		
	}
	
	public function profile(){
		if(isset($_REQUEST['user_id']))
		{
			$user_id = $_REQUEST['user_id'];
			$where = 'id="'.$user_id.'" ';
			$result = $this->API_CRUD_model->getById($where,'user');
			if(isset($result->id)){
				if($result->image)
				{
					$result->image = base_url().'assets/images/user/'.$result->image;	
				}
				$response=array('status'=>200,'message'=>'Record get success fully','result'=>array($result));
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);exit;
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter required field','result'=>array());
			echo json_encode($response);exit;
		}
	}
	
	public function update_profile(){
		if(isset($_REQUEST['user_id']) && isset($_REQUEST['fullname']) && isset($_REQUEST['email']) && isset($_REQUEST['mobile']))
		{
			$user_id = $_REQUEST['user_id'];
			$fullname = $_REQUEST['fullname'];
			$email = $_REQUEST['email'];
			$mobile = $_REQUEST['mobile'];
			
			$data = array(
				'fullname'=>$fullname,
				'email'=>$email,
				'mobile'=>$mobile
			);

			if(isset($_REQUEST['password']))
			{
				$data['password'] = $_REQUEST['password'];
			}
			
			$where = 'id="'.$user_id.'" ';
			$result = $this->API_CRUD_model->update($_REQUEST['user_id'], 'id', $data, 'user');

			if($result){
				$response=array('status'=>200,'message'=>'User profile update sucessfuly','id'=>(string)$user_id);
				echo json_encode($response);
			}else{
				$response=array('status'=>400,'message'=>'User profile fail');
				echo json_encode($response);
			}
		}else
		{
			$response=array('status'=>400,'message'=>'User profile fail');
			echo json_encode($response);	
		}
	}
}