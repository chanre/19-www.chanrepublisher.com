<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('API_CRUD_model');
		$this->load->library('email');    
	}

	public function categorylist(){
		$result = $this->API_CRUD_model->get('','category');

		$rk=array();
		foreach($result as $row){
			$row->image=base_url().'assets/images/category/'.$row->image;
			$rk[]= $row;  
		}

		if(sizeof($rk)>0){
			$response=array('status'=>200,'message'=>'Success','result'=>$rk);
		}else{
			$response=array('status'=>400,'message'=>'Sorry, we could not find any matches.<br>Please try again.','result'=>$rk);
		}
		echo json_encode($response);
	}

	public function get_book_join($where='',$field_order='id',$order='DESC')
	{
		$field = 'book.*,category.name as category_name,category.image as category_image, author.name as author_name,author.image as author_image';
		$table1 ='book'; 
		$table2 = 'category';
		$table3 = 'author';
		$joinid1 = 'category.id = book.category_id';
		$joinid2 = 'author.id = book.author_id';
		$result = $this->API_CRUD_model->get_two_join_all_record($table1, $table2, $table3, $joinid1, $joinid2, $field, $where, $field_order, $order);
		return $result;
	}

	public function get_book_data($resultr)
	{
		$rk=array();
		foreach($resultr as $row){
		
			$avg = $this->API_CRUD_model->avg_book_rating($row->id);

			$row->image = base_url().'assets/images/book/'.$row->image;
			
			$row->url = base_url().'assets/images/book/'.$row->url;
			
			$row->sample_url = base_url().'assets/images/book/'.$row->sample_url;

			if(isset($row->category_image))
			{
				$row->category_image = base_url().'assets/images/category/'.$row->category_image;
			}
			
			if(isset($row->author_image))
			{
				$row->author_image = base_url().'assets/images/category/'.$row->author_image;
			}

			if($avg->average>0){
				$row->avg_rating = number_format($avg->average,2);
			}else{
				$row->avg_rating = "0";
			}
			$rk[]= $row;
		}
		return $rk;
	}

	public function general_setting(){

		$result = $this->API_CRUD_model->get('','general_setting');

		$where = 'status=1';
		$currency = $this->API_CRUD_model->getById($where,'currency');
		if(isset($currency->id))
		{
			$data[0]['id'] = "101";
			$data[1]['id'] = "101";
			$data[2]['id'] = "103";

			$data[0]['key'] = 'currency_name';
			$data[1]['key'] = 'currency_symbol';
			$data[2]['key'] = 'currency_code';

			$data[0]['value'] = $currency->name;;
			$data[1]['value'] = $currency->symbol;
			$data[2]['value'] = $currency->code;
			
			$result = (array)$result;

			$rows = array_merge($result,$data);
		}
		else
		{
				$rows = (array)$result;
		}

		if(sizeof($result )>0){
			$response=array('status'=>200,'message'=>'Success','result'=>$rows);
		}else{
			$response=array('status'=>400,'message'=>'Sorry, we could not find any matches.<br>Please try again.','result'=>$result);
		}
		echo json_encode($response);
	}

	public function book_by_category(){
		
		if(isset($_REQUEST['category_id']))
		{
			$category_id=$_REQUEST['category_id'];
			
			$field = 'book.*,category.name as category_name,category.image as category_image, author.name as author_name,author.image as author_image';
			$where = 'category_id = "'.$category_id.'"';
			$table1 ='book'; 
			$table2 = 'category';
			$table3 = 'author';
			$joinid1 = 'category.id = book.category_id';
			$joinid2 = 'author.id = book.author_id';
			$result = $this->API_CRUD_model->get_two_join_allrecord($table1, $table2, $table3, $joinid1, $joinid2, $field,$where);

			if(count($result)>0)
			{ 
				$result_data = $this->get_book_data($result);
				if(sizeof($result_data)>0){
					$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
				}else{
					$response=array('status'=>400,'message'=>'Record not found','result'=>array());
				}
			}else
			{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);exit;
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please select category','result'=>array());
			echo json_encode($response);exit;
		}
	}

	public function autherlist(){
		$result=$this->API_CRUD_model->get('','author');

		$rk=array();
		foreach($result as $row){
			$row->image=base_url().'assets/images/author/'.$row->image;
			$rk[]= $row;  
		}

		if(sizeof($rk)>0){
			$response=array('status'=>200,'message'=>'Record get successfully','result'=>$rk);
		}else{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
		}
		echo json_encode($response);
	}

	public function book_by_author(){
		
		if(isset($_REQUEST['author_id']))
		{
			$where='author_id="'.$_REQUEST['author_id'].'"';
			$field = 'book.*,category.name as category_name,category.image as category_image, author.name as author_name,author.image as author_image';
			
			$table1 ='book'; 
			$table2 = 'category';
			$table3 = 'author';
			$joinid1 = 'category.id = book.category_id';
			$joinid2 = 'author.id = book.author_id';
			$result = $this->API_CRUD_model->get_two_join_allrecord($table1, $table2, $table3, $joinid1, $joinid2, $field,$where);

			if(count($result) > 0)
			{
				$result_data = $this->get_book_data($result);
				if(sizeof($result_data)>0){
					$response=array('status'=>200,'message'=>'Record get successfully','result'=>$result_data);
				}else{
					$response=array('status'=>400,'message'=>'Record not found','result'=>array());
				}
				echo json_encode($response);exit;
			}else
			{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
				echo json_encode($response);exit;
			}
		}else
		{
			$response=array('status'=>400,'message'=>'Please select author','result'=>array());
			echo json_encode($response);exit;
		}
	}

	public function readcount_by_author(){
		if(isset($_REQUEST['author_id'])){	
			
			$where='author_id="'.$_REQUEST['author_id'].'"';
			$field = 'book.*,sum(readcnt) as readcount, sum(download) as totla_download,category.name as category_name,category.image as category_image, author.name as author_name,author.image as author_image';
			$table1 ='book'; 
			$table2 = 'category';
			$table3 = 'author';
			$joinid1 = 'category.id = book.category_id';
			$joinid2 = 'author.id = book.author_id';
			$result = $this->API_CRUD_model->get_two_join_allrecord($table1, $table2, $table3, $joinid1, $joinid2, $field,$where);
			;
			if(sizeof($result)>0){
				if(isset($result[0]->id)){
					$result_data = $this->get_book_data($result);
					$response=array('status'=>200,'message'=>'Record get successfully','result'=>$result_data);
				}else
				{
					$response=array('status'=>400,'message'=>'Record not found','result'=>array());	
				}
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);exit;
		}else
		{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			echo json_encode($response);exit;
		}
	}

	public function booklist(){
		
		$field = 'book.*,category.name as category_name,category.image as category_image, author.name as author_name,author.image as author_image';
		$table1 ='book'; 
		$table2 = 'category';
		$table3 = 'author';
		$joinid1 = 'category.id = book.category_id';
		$joinid2 = 'author.id = book.author_id';
		$result = $this->API_CRUD_model->get_two_join_allrecord($table1, $table2, $table3, $joinid1, $joinid2, $field,'');

		if(count($result)>0)
		{ 
			$result_data = $this->get_book_data($result);
			if(sizeof($result_data)>0){
				$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
		}else
		{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
		}
		echo json_encode($response);exit;
	}

	public function bookdetails(){
		if(isset($_REQUEST['book_id']) && $_REQUEST['book_id'] > 0){
			$book_id = $_REQUEST['book_id'];
			$user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id']  : 0;
			$is_buy = 0;
			
			$where='book.id="'.$book_id.'"';
			$field = 'book.*,category.name as category_name,category.image as category_image, author.name as author_name,author.image as author_image';
			$table1 ='book'; 
			$table2 = 'category';
			$table3 = 'author';
			$joinid1 = 'category.id = book.category_id';
			$joinid2 = 'author.id = book.author_id';
			$result = $this->API_CRUD_model->get_two_join_allrecord($table1, $table2, $table3, $joinid1, $joinid2, $field,$where);

			if(count($result)>0)
			{ 
				if($user_id){
					$where = 'user_id='.$user_id.' AND book_id='.$book_id;
					// $field = '*';
					//$table1 ='transaction'; 
					//$table2 ='package'; 
					//$joinid1 = 'transaction.package_id = package.id';
					$get_tradetail = $this->API_CRUD_model->getById($where,'transaction');

					if(isset($get_tradetail->id))
					{
						$is_buy = 1;				
					}
					// $resultr_tra=$get_tradetail[0];
					// foreach($result as $ra){
					// 	if(isset($resultr_tra->book_id))
					// 	{
					// 		$books_id = explode(',', $resultr_tra->book_id);
					// 		if(in_array($book_id, $books_id))
					// 		{
								
					// 		}
					// 	}
					// }	
				}
			}
			$result_data = $this->get_book_data($result);
			$result_data[0]->is_buy = $is_buy;

			if(sizeof($result_data)>0){
				$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);
		}else
		{
			$response=array('status'=>400,'message'=>'Please select book id','result'=>array());
			echo json_encode($response);
		}
	}

	public function popularbooklist(){
		
		$result = $this->get_book_join('','readcnt', 'DESC');
		if(count($result)>0)
		{ 
			$result_data = $this->get_book_data($result);
			if(sizeof($result_data)>0){
				$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
		}else
		{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
		}
		echo json_encode($response);exit;
	}

	public function free_paid_booklist(){
	
		$is_paid = isset($_REQUEST['is_paid']) ? $_REQUEST['is_paid'] : 0;
		$where='is_paid="'.$is_paid.'" ';
		$result = $this->get_book_join($where,'readcnt', 'DESC');

		if(count($result)>0)
		{ 
			$result_data = $this->get_book_data($result);
			if(sizeof($result_data)>0){
				$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
		}else
		{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
		}
		echo json_encode($response);exit;
	}

	public function related_item(){

		if(isset($_REQUEST['category_id']))
		{
			$category_id = $_REQUEST['category_id'];
			$where = 'category_id="'.$category_id.'" ';

			$result = $this->get_book_join($where,'readcnt', 'DESC');
			if(count($result)>0)
			{ 
				$result_data = $this->get_book_data($result);
				if(sizeof($result_data)>0){
					$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
				}else{
					$response=array('status'=>400,'message'=>'Record not found','result'=>array());
				}
			}else
			{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);exit;
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter category id','result'=>array());	
			echo json_encode($response);exit;
		}
	}

	public function newarriaval(){

		$result = $this->get_book_join('','id', 'DESC');

		if(count($result)>0)
		{ 
			$result_data = $this->get_book_data($result);
			if(sizeof($result_data)>0){
				$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
		}else
		{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
		}
		echo json_encode($response);exit;
	}

	public function feature_item(){

		$where='is_feature="yes" ';
		$result = $this->get_book_join($where,'id', 'DESC');

		if(count($result)>0)
		{ 
			$result_data = $this->get_book_data($result);
			if(sizeof($result_data)>0){
				$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
			}else{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
		}else
		{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
		}
		echo json_encode($response);exit;
	}

	public function add_view(){
		if($_REQUEST['book_id']){
			
			$id=$_REQUEST['book_id'];
			$add_point=1;
			$result = $this->API_CRUD_model->updateByIdWithcount($id,'readcnt',$add_point,'book');
			
			if($result){
				$response=array('status'=>200,'message'=>'Success','result'=>array());
			}else{
				$response=array('status'=>400,'message'=>'View not update','result'=>array());
			}
			echo json_encode($response);exit;
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter book id','result'=>array());	
			echo json_encode($response);
		}
	}

	public function add_download(){
		
		$user_id=$_REQUEST['user_id'];
		$id=$_REQUEST['book_id'];
		$add_point=1;
		
		$result = $this->API_CRUD_model->updateByIdWithcount($id,'download',$add_point,'book');

		$where_down='user_id='.$user_id.' and book_id="'.$id.'"';
		$resultr_down=$this->API_CRUD_model->getById($where_down,'download');

		$data = array(
			'user_id'=>$user_id,
			'book_id'=>$id
		);

		if(isset($resultr_down->id)){			
			$response=array('status'=>201,'message'=>'Already download');
		}else{
			$id = $this->API_CRUD_model->insert($data,'download');
			$response=array('status'=>200,'message'=>'Download Success');
		}

		echo json_encode($response);
	}

	public function alldownload(){
			
		if(isset($_REQUEST['user_id'])){	
			$user_id=$_REQUEST['user_id'];
			$where_tra='user_id='.$user_id.' ';
			$result = $this->get_book_join('','id', 'DESC');
			$resultr_con=$this->API_CRUD_model->get($where_tra,'download');

			$rk=array();
			foreach($result as $row){
				$avg = $this->API_CRUD_model->avg_book_rating($row->id);

				$row->image = base_url().'assets/images/book/'.$row->image;
				
				$row->url = base_url().'assets/images/book/'.$row->url;
				
				$row->sample_url = base_url().'assets/images/book/'.$row->sample_url;

				if(isset($row->category_image))
				{
					$row->category_image = base_url().'assets/images/category/'.$row->category_image;
				}
				
				if(isset($row->author_image))
				{
					$row->author_image = base_url().'assets/images/category/'.$row->author_image;
				}

				if($avg->average>0){
					$row->avg_rating = number_format($avg->average,2);
				}else{
					$row->avg_rating = "0";
				}

				foreach($resultr_con as $racon){
					if($racon->book_id == $row->id){	
						$rk[]= $row; 
					}
				}
			}

			if(sizeof($rk)>0){
				$response=array('status'=>200,'message'=>'Success','result'=>$rk);
			}else{
				$response=array('status'=>400,'message'=>'Sorry, we could not find any matches.<br>Please try again.','result'=>'');
			}
			echo json_encode($response);
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter user id','result'=>'');
			echo json_encode($response);
		}
	}

	public function add_comment(){
		if(isset($_REQUEST['book_id']) && isset($_REQUEST['user_id']) && isset($_REQUEST['comment']))
		{
			$book_id=$_REQUEST['book_id'];
			$user_id=$_REQUEST['user_id'];
			$comment=$_REQUEST['comment'];

			$data = array(
				'book_id'=>$_REQUEST['book_id'],
				'comment'=>$_REQUEST['comment'],
				'user_id'=>$_REQUEST['user_id']
			);

			$comment_id=$this->API_CRUD_model->insert($data,'comment');

			$response=array('status'=>200,'message'=>'Add comment sucessfuly','id'=>(string)$comment_id);
			echo json_encode($response);
		}else
		{
			$response=array('status'=>400,'message'=>'Please enter required field','id'=>"");
			echo json_encode($response);
		}
	}

	public function view_comment(){
		if(isset($_REQUEST['book_id'])){

			$book_id=$_REQUEST['book_id'];

			$where='book_id="'.$book_id.'"';

			$field = 'user.fullname,comment.*';
			$table1 ='comment'; 
			$table2 ='user'; 
			$joinid1 = 'user.id = comment.user_id';
			$result = $this->API_CRUD_model->get_join_allrecord($table1, $table2, $joinid1, $field, $where,'comment.id','DESC');

			if(sizeof($result)>0){
				$response=array('status'=>200,'message'=>'Success','result'=>$result);
			}else{
				$response=array('status'=>400,'message'=>'Sorry, we could not find any matches.Please try again.','result'=>(object)array());
			}
			echo json_encode($response);
		}
	}

	public function add_transaction(){
		
		$author_id = isset($_REQUEST['author_id']) ? $_REQUEST['author_id'] : '';
		$book_id = isset($_REQUEST['book_id']) ? $_REQUEST['book_id'] : '';
		$user_id = isset($_REQUEST['user_id']) ?  $_REQUEST['user_id'] :'';
		$amount =  isset($_REQUEST['amount']) ? $_REQUEST['amount']: '';
		$currency_code = isset($_REQUEST['currency_code']) ? $_REQUEST['currency_code'] : "";
		$description = isset($_REQUEST['description']) ? $_REQUEST['description'] :'';
		$payment_id = isset($_REQUEST['payment_id']) ? $_REQUEST['payment_id'] :'';
		$state = isset($_REQUEST['state']) ? $_REQUEST['state'] :'';

		$settinglist = $this->API_CRUD_model->get('','general_setting');
	    foreach($settinglist as $set){
	        $setn[$set->key]=$set->value;
		}

		$author_commission = $setn['author_commission'];

		$author_commission_amount = ($amount * $author_commission) / 100;

		$data = array(
			'user_id'=>$user_id,
			'author_id'=>$author_id,
			'book_id'=>$book_id,
			'author_commission_amount' => $author_commission_amount,
			'currency_code'=>$currency_code,
			'description'=>$description,
			'state'=>$state,
			'payment_id'=>$payment_id,
			'amount'=>$amount
		);
		
		$id = $this->API_CRUD_model->insert($data,'transaction');
		$result['id'] = $id;
		$data['id'] =  $id;
		$where_book = 'id='.$book_id;
		$mail['book'] = $this->API_CRUD_model->getById($where_book,'book');

		$where_author = 'id='.$author_id;
		$mail['author'] = $this->API_CRUD_model->getById($where_author,'author');

		$where_user = 'id='.$user_id;
		$mail['user'] = $this->API_CRUD_model->getById($where_user,'user');

		$mail['transaction'] = $data;

		$mail['setn'] = $setn;
		// p($mail);	
		$book_invoice = $this->load->view("admin/email/book_invoice",$mail,true);
		// p($book_invoice);
		$email = $mail['user']->email;
		$subject = "Book Invoice #EB".date('Ym').'T'.$data['id'];
		$this->send_mail($book_invoice,$email,$subject);

		$response=array('status'=>200,'message'=>'Transaction successfully','result'=>$result);
		echo json_encode($response);exit;
	}
	
	public function purchaselist(){
		
		if(isset($_REQUEST['user_id']))
		{
			$field = 'book.*, transaction.currency_code,transaction.amount,author.name as author_name,author.image as author_image';
			$table1 ='transaction'; 
			$table2 = 'book';
			$table3 = 'author';
			$joinid1 = 'book.id = transaction.book_id';
			$joinid2 = 'author.id = book.author_id';
			$where = 'transaction.user_id='.$_REQUEST['user_id'];
			$result = $this->API_CRUD_model->get_two_join_allrecord($table1, $table2, $table3, $joinid1, $joinid2, $field,$where);

			if(count($result)>0)
			{ 
				$result_data = $this->get_book_data($result);
				if(sizeof($result_data)>0){
					$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
				}else{
					$response=array('status'=>400,'message'=>'Record not found','result'=>array());
				}
			}else
			{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);exit;
		}else
		{
			$response=array('status'=>400,'message'=>'Please enter required field','id'=>"");	
			echo json_encode($response);exit;
		}
	}


	public function add_continue_read(){

		if(isset($_REQUEST['user_id']) && isset($_REQUEST['book_id']))
		{
			$user_id = $_REQUEST['user_id'];
			$book_id = $_REQUEST['book_id'];

			$where_tra='user_id='.$user_id.' AND book_id='.$book_id.' ';
			$resultr_con=$this->API_CRUD_model->getById($where_tra,'continue_read');

			if(isset($resultr_con->id)){
				$response=array('status'=>400,'message'=>'Already Added','User_id'=>'');
			}else{
				$data = array(
					'user_id'=>$user_id,
					'book_id'=>$book_id
				);

				$continue_read_id = $this->API_CRUD_model->insert($data,'continue_read');
				$response=array('status'=>200,'message'=>'Add sucessfuly','id'=>(string)$continue_read_id);
			}
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter required field','id'=>"");	
		}
		echo json_encode($response);
	}

	public function continue_read(){
		if(isset($_REQUEST['user_id'])){
			$user_id = $_REQUEST['user_id'];
			$result=$this->API_CRUD_model->allbook($user_id);
			if(count($result)>0)
			{ 
				$result_data = $this->get_book_data($result);
				if(sizeof($result_data)>0){
					$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
				}else{
					$response=array('status'=>400,'message'=>'Record not found','result'=>array());
				}
			}else
			{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);exit;
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter user id','result'=>array());	
			echo json_encode($response);exit;
		}
	}

	public function add_bookmark(){

		if(isset($_REQUEST['user_id']) && isset($_REQUEST['book_id']))
		{
			$user_id = $_REQUEST['user_id'];
			$book_id = $_REQUEST['book_id'];

			$where_tra='user_id='.$user_id.' AND book_id='.$book_id.' ';
			$resultr_con=$this->API_CRUD_model->getById($where_tra,'bookmark');

			if(isset($resultr_con->id)){
				$response=array('status'=>400,'message'=>'Already Added','User_id'=>'');
			}else{
				$data = array(
					'user_id'=>$user_id,
					'book_id'=>$book_id
				);

				$bookmark_id = $this->API_CRUD_model->insert($data,'bookmark');
				$response=array('status'=>200,'message'=>'Add sucessfuly','id'=>(string)$bookmark_id);
			}
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter required field','id'=>"");	
		}
		echo json_encode($response);
	}

	public function all_bookmark(){

		if(isset($_REQUEST['user_id'])){
			$user_id = $_REQUEST['user_id'];

			$result=$this->API_CRUD_model->allBookWithBookmark($user_id);
			
			if(count($result)>0)
			{ 
				$result_data = $this->get_book_data($result);
				if(sizeof($result_data)>0){
					$response=array('status'=>200,'message'=>'Record get success','result'=>$result_data);
				}else{
					$response=array('status'=>400,'message'=>'Record not found','result'=>array());
				}
			}else
			{
				$response=array('status'=>400,'message'=>'Record not found','result'=>array());
			}
			echo json_encode($response);exit;
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter user id','result'=>array());	
			echo json_encode($response);exit;
		}
	}

	public function userlist(){

		$resultr=$this->API_CRUD_model->get('','user');

		if(sizeof($resultr)>0){
			$response=array('status'=>200,'message'=>'Success','result'=>$resultr);
		}else{
			$response=array('status'=>400,'message'=>'Sorry, we could not find any matches.<br>Please try again.','result'=>array());
		}
		echo json_encode($response);
	}

	public function add_rating(){

		if(isset($_REQUEST['book_id']) && isset($_REQUEST['user_id']) && isset($_REQUEST['rating']))
		{
			$book_id = $_REQUEST['book_id'];
			$user_id = $_REQUEST['user_id'];
			$ratingData  = $_REQUEST['rating'];
			$where   ='book_id="'.$book_id.'" and user_id="'.$user_id.'"';
			$rating = $this->API_CRUD_model->getById($where,'rating');

			$data = array(
				'book_id' => $book_id,
				'user_id'=>$user_id,
				'rating'=>$ratingData);

			if(isset($rating->id)){
				$where ='rating_id="'.$rating->id.'"';	
				$this->API_CRUD_model->update($rating->id,'id',$data,'rating');
			}else{
				$this->API_CRUD_model->insert($data,'rating');
			}

			$response=array('status'=>200,'message'=>'Success');
			echo json_encode($response);
		}
		else
		{
			$response=array('status'=>400,'message'=>'Please enter required field');
			echo json_encode($response);
			
		}
	}

	public function get_package(){
		$results=$this->API_CRUD_model->get('','package');
		$data=  [];
			foreach($results as $value){
				if(strlen($value->book_id) > 0)
				{
					$book_id = explode(',', $value->book_id);	
				}
				$book_name = '';
				$result = $this->API_CRUD_model->get_book_ids($book_id);
				$rk = $this->get_book_data($result);
				$value->books = $rk;
				$data[] = $value;
			}
			

		if(sizeof($data)>0){
			$response=array('status'=>200,'message'=>'Success','result'=>$data);
		}else{
			$response=array('status'=>400,'message'=>'Recrd not found','result'=>array());
		}
		echo json_encode($response);
	}


	public function send_mail($message,$email,$subject)
	{
		$smtpWhere='id="1"';
		$smtp_detail = $this->API_CRUD_model->getById($smtpWhere,'smtp_setting');
		$emailconfig = get_smtp_setting();
		
		$this->email->initialize($emailconfig);
		$this->email->from($smtp_detail->from_email, $smtp_detail->from_name);
		$this->email->to($email);
		$this->email->set_mailtype('html');
		$this->email->subject($subject);
		$this->email->message($message);  
		$this->email->send();

	}

	public function get_ads_banner(){
		$result=$this->API_CRUD_model->get('','ads_banner');

		$rk=array();
		foreach($result as $row){
			$row->image=base_url().'assets/images/banner/'.$row->image;
			$rk[]= $row;  
		}

		if(sizeof($rk)>0){
			$response=array('status'=>200,'message'=>'Record get successfully','result'=>$rk);
		}else{
			$response=array('status'=>400,'message'=>'Record not found','result'=>array());
		}
		echo json_encode($response);
	}

	public function checkbookmark()
	{
		if(isset($_REQUEST['book_id']) && isset($_REQUEST['user_id']))
		{
			$where = 'user_id='.$_REQUEST['user_id'].' AND book_id='.$_REQUEST['book_id'].'';
			$result=$this->API_CRUD_model->getById($where,'bookmark');

			if(isset($result->id)){
				$response=array('status'=>200,'message'=>'This book is bookmarked');
			}else{
				$response=array('status'=>400,'message'=>'This book is not bookmarked');
			}
			echo json_encode($response);
		}else{
			$response=array('status'=>400,'message'=>'Please enter required field');
			echo json_encode($response);
		}	
	}
}