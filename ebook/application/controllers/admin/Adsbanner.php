<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adsbanner extends CI_Controller{

	public function __construct() {
		parent::__construct();
		$CI =& get_instance();
		frontendcheck();
	}

	public function index(){
		$data['ads_bannerlist'] = $this->CRUD_model->get('','ads_banner','id','desc');
		$this->load->view("admin/ads_banner/list",$data);
	}

	public function add(){
		$this->load->view("admin/ads_banner/add");
	}

	public function save(){
		$this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $errors = $this->form_validation->error_array();
            sort($errors);
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
			{ 
				$ads_banner_image = $this->CRUD_model->imageupload($_FILES['image'],'image', FCPATH . 'assets/images/banner');
			}else
			{
				$res=array('status'=>'400','message'=>array('Please select banner image.'));
				echo json_encode($res);exit;	
			}

			$data = array(
				'name' => $_POST['name'],
				'image' => $ads_banner_image,
			);

			$id=$this->CRUD_model->insert($data,'ads_banner');
			$res=array('status'=>'200','message'=>'Sucessfully updated','id'=>$id);
			echo json_encode($res);exit;
		}
	}

	public function edit(){
		$where='id="'.$_GET['id'].'"';
		$data['ads_banner'] = $this->CRUD_model->getById($where,'ads_banner');
		$this->load->view("admin/ads_banner/edit",$data);
	}

	public function update(){
		$this->form_validation->set_rules('name', 'Banner Name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
             $errors = $this->form_validation->error_array();
            sort($errors); 	
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {
			$name=$_POST['name'];
			if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
				$ads_banner_image=$this->CRUD_model->imageupload($_FILES['image'],'image', FCPATH . 'assets/images/banner');
				
			}else{
				$ads_banner_image=$_POST['ads_bannerimage'];	
			}

			$data = array(
				'name' => $name,
				'image' => $ads_banner_image
			);
			
			$cat_id=$this->CRUD_model->update($_POST['id'],'id',$data,'ads_banner');	

			$res=array('status'=>'200','message'=>'Sucessfully updated','id'=>$cat_id);
			echo json_encode($res);exit;
		}
	}

	public function fetch_data(){  

    	$table = "ads_banner";
	  	$select_column = array("id","image","name");  
	  	$search = array("name");  
	  	$order_column = array(null, "name");  

       	$fetch_data = $this->CRUD_model->make_datatables($table, $select_column,$order_column,$search);  
       	$data = array();  
       	foreach($fetch_data as $key=> $row)  
       	{  
            $sub_array = array();  
           	$sub_array[] = '<img src="'.base_url().'assets/images/banner/'.$row->image.'"  width="50" height="35" />';  
            $sub_array[] = string_cut($row->name,15);
            
            $sub_array[] = '<a href="'.base_url().'admin/adsbanner/edit?id='.$row->id.'">Edit</a> | <a href="javaScript:void(0)" onclick="delete_record('.$row->id.',\'ads_banner\')">Delete</a>';
            $data[] = $sub_array;  
       	}
       	$output = array(  
            "draw"             => intval($_POST["draw"]),  
            "recordsTotal"     => $this->CRUD_model->get_all_data($table),  
            "recordsFiltered"  => $this->CRUD_model->get_filtered_data($table,$select_column,$order_column,$search=null),  
            "data"             => $data  
       	);  
       	echo json_encode($output);  
    }
}
