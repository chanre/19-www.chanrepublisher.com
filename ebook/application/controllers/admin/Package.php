<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends CI_Controller{

	public function __construct() {
		parent::__construct();
	}

	/*====================== Package =============================*/

	public function index(){
		$data['packages'] = $this->CRUD_model->get('','package');

		foreach ($data['packages'] as $value) {
			if(strlen($value->book_id) > 0)
			{
				$book_id = explode(',', $value->book_id);	
			}
			$book_name = '';
			$result = $this->CRUD_model->get_book_id($book_id);

			foreach ($result as $row) {
				if($book_name){
					$book_name .=  ', '.$row->title;
				}else
				{
					$book_name .=  $row->title;
				}
			} 
			$value->book_name = $book_name;
		}
		
		$this->load->view("admin/package/list",$data);
	}

	public function add(){
		$data['booklist'] = $this->CRUD_model->get('','book');
		$this->load->view("admin/package/add",$data);
	}

	public function save(){
		$this->form_validation->set_rules('package_name', 'Package Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $errors = $this->form_validation->error_array();
            sort($errors);
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {
        	$book_id= isset($_POST['book_id']) ? $_POST['book_id'] : array();

			$data= $_POST;
			
			if(isset($_FILES['package_image']['name']) && $_FILES['package_image']['name'] != '')
			{
				$data['package_image'] = $this->CRUD_model->imageupload($_FILES['package_image'],'package_image', FCPATH . 'assets/images/package');
			}
			else
			{
				$res=array('status'=>'400','message'=>array('Please select package image'));
				echo json_encode($res);exit;
			}

			if(count($book_id)> 0)
			{
				$data['book_id'] = implode(',', $book_id);
			}else
			{
				$array  = array('status'=>400,'message'=>array('Please select book'));
         		echo json_encode($array);exit;
			}

			$id = $this->CRUD_model->insert($data,'package');
			
			if($id){
				$res=array('status'=>'200','message'=>'New package create.','id'=>$id);
				echo json_encode($res);exit;
			}else{
				$res=array('status'=>'400','message'=>'Package not save ');
				echo json_encode($res);exit;
			}
		}
	}

	public function edit(){
		$id=$_GET['id'];
		$where='id="'.$id.'"';
		$data['booklist'] = $this->CRUD_model->get('','book');
		$data['package'] = $this->CRUD_model->getById($where,'package');

		$this->load->view("admin/package/edit",$data);
	}
	
	public function update(){

		$this->form_validation->set_rules('package_name', 'Package Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $errors = $this->form_validation->error_array();
            sort($errors);
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {

			$package_name= isset($_POST['package_name']) ? $_POST['package_name'] : '';
			$book_id= isset($_POST['book_id']) ? $_POST['book_id'] : 0;
			$price= isset($_POST['price']) ? $_POST['price'] : 0;
			$no_of_book= isset($_POST['no_of_book']) ? $_POST['no_of_book'] : 0;

			if (isset($_FILES['package_image']) && !empty($_FILES['package_image']['name'])) {
				
				$package_image=$this->CRUD_model->imageupload($_FILES['package_image'],'package_image', FCPATH . 'assets/images/package');	
			}else{
				$package_image=$_POST['packageimage'];	
			}
			$id=$_POST['id'];


			if(count($book_id)> 0)
			{
				$books_id = implode(',', $book_id);
			}

			$data = array(
				'package_name' => $package_name,
				'book_id' => $books_id,
				'price '=> $price,
				'no_of_book '=> $no_of_book,
				'package_image '=> $package_image,
			);

			$id=$this->CRUD_model->update($id,'id',$data,'package');

			if($id){
				$res=array('status'=>'200','message'=>'Update package success','id'=>$id);
				echo json_encode($res);exit;
			}else{
				$res=array('status'=>'400','message'=>'fail');
				echo json_encode($res);exit;
			}
		}
	}
	/*======================End Package=============================*/


	public function fetch_data(){  

    	$table = "package";
	  	$select_column = array("id","package_name", "price","book_id","package_image","created_at");  
	  	$order_column = array(null, "package_name", "price","book_id","package_image","created_at");  
	  	$search = array("package_name", "price","book_id","package_image","created_at");

       	$fetch_data = $this->CRUD_model->make_datatables($table, $select_column,$order_column,$search);  
       	$data = array();  
       	foreach($fetch_data as $key=> $row)  
       	{  

       		if(strlen($row->book_id) > 0)
			{
				$book_id = explode(',', $row->book_id);	
			}
			$book_name = '';
			$result = $this->CRUD_model->get_book_id($book_id);

			foreach ($result as $rows) {
				if($book_name){
					$book_name .=  ', '.$rows->title;
				}else
				{
					$book_name .=  $rows->title;
				}
			} 
			$row->book_name = $book_name;

            $sub_array = array();  

			
           	$sub_array[] = '<img src="'.base_url().'assets/images/package/'.$row->package_image.'"  width="50" height="35" />';  

            $sub_array[] = string_cut($row->package_name,15);
            $sub_array[] = $row->price;  
            $sub_array[] = string_cut($row->book_name,15);
            $sub_array[] = dateformate($row->created_at);
            $sub_array[] = '<a href="'.base_url().'admin/package/edit?id='.$row->id.'">Edit</a> | <a href="javaScript:void(0)" onclick="delete_record('.$row->id.',\'package\')">Delete</a>';
            $data[] = $sub_array;  
       	}
       	$output = array(  
            "draw"             => intval($_POST["draw"]),  
            "recordsTotal"     => $this->CRUD_model->get_all_data($table),  
            "recordsFiltered"  => $this->CRUD_model->get_filtered_data($table,$select_column,$order_column,$search=null),  
            "data"             => $data  
       	);  
       	echo json_encode($output);  
    }
}
