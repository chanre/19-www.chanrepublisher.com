<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller{

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view("admin/notification/index");
	}

	public function save(){
		$this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('message', 'Message ', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $errors = $this->form_validation->error_array();
            sort($errors);
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {

			$setting= settings_data();
			foreach($setting as $set)
			{
				$setn[$set->key]=$set->value;
			}

			$ONESIGNAL_APP_ID=$setn['onesignal_apid'];
			$ONESIGNAL_REST_KEY=$setn['onesignal_rest_key'];
			$big_picture=$_FILES['video_thumbnail']['name'];
			$tpath2= FCPATH . 'assets/images/book/';
			$config = array(
				'allowed_types' => 'jpg|jpeg|gif|png',
				'upload_path' => FCPATH . 'assets/images/book'
			);
			$this->load->library('upload');
			$this->upload->initialize($config);
			$this->upload->do_upload('video_thumbnail');

			$content = array(
				"en" => $_POST['message']                                                 
			);

			if(isset($_FILES['video_thumbnail']['name']))
			{
				$_FILES['video_thumbnail']['name'];
				$file_path = base_url().'assets/images/book/'.$big_picture;
				$fields = array(
					'app_id' =>  $ONESIGNAL_APP_ID,
					'included_segments' => array('All'),                                            
					'data' => array("foo" => "bar"),
					'headings'=> array("en" => $_POST['title']),
					'contents' => $content,
					'big_picture' =>$file_path                    
				);
			}else
			{
				$file_path = '';  
				$fields = array(
					'app_id' => $ONESIGNAL_APP_ID,
					'included_segments' => array('All'),   
					'data' => array("foo" => "bar"),
					'headings'=> array("en" => $_POST['title']),
					'contents' => $content,
				);
			}

			$fields = json_encode($fields);    

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
				'Authorization: Basic '.$ONESIGNAL_REST_KEY));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

			$response = curl_exec($ch);

			curl_close($ch);
			print_r($response);

		}
	}
	
}
