<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller{

	public function __construct() {
		parent::__construct();
		frontendcheck();
	}

	public function index(){
		$data['bookslist'] = $this->CRUD_model->get('','book');
		$this->load->view("admin/book/list",$data);
	}

	public function add(){
		$data['booklist'] = $this->CRUD_model->get('','book');
		$data['authorlist'] = $this->CRUD_model->get('','author');
		$data['categorylist'] = $this->CRUD_model->get('','category');
		$this->load->view("admin/book/add",$data);
	}

	public function save(){
		$this->form_validation->set_rules('title', 'Book Name', 'required');
        $this->form_validation->set_rules('is_paid', 'Book Cost ', 'required');
        $this->form_validation->set_rules('category_id', 'Category', 'required');
        $this->form_validation->set_rules('author_id', 'Author', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $errors = $this->form_validation->error_array();
            sort($errors);
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {
			$data = $_POST;
			$data['image'] = '';
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
			{
				$data['image'] = $this->CRUD_model->imageupload($_FILES['image'],'image', FCPATH . 'assets/images/book');
			}else
			{
				$array  = array('status'=>400,'message'=>array('Please Select Book Cover Poster'));
         		echo json_encode($array);exit;
			}


			if (isset($_FILES['sample_url']) && !empty($_FILES['sample_url']['name'])) {
				$data['sample_url'] = $this->CRUD_model->imageupload($_FILES['sample_url'],'sample_url', FCPATH . 'assets/images/book');
			}
			else
			{
				$array  = array('status'=>400,'message'=>array('Please select sample book image'));
         		echo json_encode($array);exit;
			}

			if (isset($_FILES['url']) && !empty($_FILES['url']['name'])) {
				$data['url'] = $this->CRUD_model->imageupload($_FILES['url'],'url', FCPATH . 'assets/images/book');
			}else
			{
				$array  = array('status'=>400,'message'=>array('Please Select Full Book'));
         		echo json_encode($array);exit;
			}

			
			$id = $this->CRUD_model->insert($data,'book');

			if($id){
				$res=array('status'=>'200','message'=>'Book added successfully.','id'=>$id);
				echo json_encode($res);exit;
			}else{
				$res=array('status'=>'400','message'=>'fail');
				echo json_encode($res);exit;
			}
		}
	}	

	public function edit(){
		$id=$_GET['id'];
		$data['categorylist'] = $this->CRUD_model->get('','category');
		
		$where='id="'.$id.'"';
		$data['booklist'] = $this->CRUD_model->getById($where,'book');
		// p($data);
		$data['authorlist'] = $this->CRUD_model->get('','author');
		$this->load->view("admin/book/edit",$data);
	}

	public function update(){
		$this->form_validation->set_rules('title', 'Book Name', 'required');
        $this->form_validation->set_rules('is_paid', 'Book Cost ', 'required');
        //$this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('category_id', 'Category', 'required');
        $this->form_validation->set_rules('author_id', 'Author', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $errors = $this->form_validation->error_array();
            sort($errors);
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {
			$data = $_POST;
			
			if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
				$data['image'] = $this->CRUD_model->imageupload($_FILES['image'],'image', FCPATH . 'assets/images/book');
			}else{
				$data['image'] =$_POST['inputbookcover'];	
			}


			if (isset($_FILES['sample_url']) && !empty($_FILES['sample_url']['name'])) {
				$data['sample_url'] = $this->CRUD_model->imageupload($_FILES['sample_url'],'sample_url', FCPATH . 'assets/images/book');
			}else{
				$data['sample_url'] = $_POST['inputsamplebook'];	
			}

			if (isset($_FILES['url']) && !empty($_FILES['url']['name'])) {
				$data['url'] = $this->CRUD_model->imageupload($_FILES['url'],'url', FCPATH . 'assets/images/book');
			}else{
				$data['url'] = $_POST['inputfullbook'];	
			}


			unset($data['inputbookcover']);
			unset($data['inputsamplebook']);
			unset($data['inputfullbook']);
			
			$id=$this->CRUD_model->update($data['id'],'id',$data,'book');

			if($id){
				$res=array('status'=>'200','message'=>'Book added successfully.','id'=>$id);
				echo json_encode($res);exit;
			}else{
				$res=array('status'=>'400','message'=>'Please try again');
				echo json_encode($res);exit;
			}
		}
	}

	public function fetch_data(){  

    	$table = "book";  
	  	$select_column = array("id","author_id","title", "readcnt", "image", "price", "download");  
	  	$order_column = array(null, "title", "readcnt", null, null);  
	  	$search = array('title','readcnt','download');

	  	$where = '';
	  	if($_SESSION['role_id'] == 2)
	  	{
	  		$author_id = $_SESSION['id']; 
	  		$where = 'author_id='.$author_id;
	  	}


       	$fetch_data = $this->CRUD_model->make_datatables($table, $select_column,$order_column,$search,$where);  
       	$data = array();  
       	foreach($fetch_data as $key=> $row)  
       	{  
       		$where = 'id='.$row->author_id;
       		$author = $this->CRUD_model->getById($where,'author');
       		$author_name = '';
       		$author_name = isset($author->name) ? $author->name : '';



            $sub_array = array();  
           	$sub_array[] = '<img src="'.base_url().'assets/images/book/'.$row->image.'"  width="50" height="35" />';  
            $sub_array[] = $row->title;  
            $sub_array[] = $author_name;  
            $sub_array[] = $row->readcnt;  
            $sub_array[] = $row->download;  
            $sub_array[] = $row->price;  
            
            $sub_array[] = '<a href="'.base_url().'admin/book/edit?id='.$row->id.'">Edit</a> | <a href="javaScript:void(0)" onclick="delete_record('.$row->id.',\'book\')">Delete</a>';
            $data[] = $sub_array;  
       	}
       	$output = array(  
            "draw"             => intval($_POST["draw"]),  
            "recordsTotal"     => $this->CRUD_model->get_all_data($table),  
            "recordsFiltered"  => $this->CRUD_model->get_filtered_data($table,$select_column,$order_column,$search=null,$where),  
            "data"             => $data  
       	);  
       	echo json_encode($output);  
    }
}
