<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

	public function __construct() {
		parent::__construct();
		$CI =& get_instance();
	}

	public function index(){
		/*for Book Count*/
		$result = $this->CRUD_model->get('','book');
		$data['wallpaper']=sizeof($result);

		/*for category Count*/
		$result = $this->CRUD_model->get('','category');
		$data['category']=sizeof($result);

		/*Register User*/
		$result = $this->CRUD_model->get('','user');
		$data['register_user']=sizeof($result);


		/*Autohr*/
		$where = 'role_id=2';
		$autohr = $this->CRUD_model->get($where,'author');
		$data['autohr']=sizeof($autohr);

		/*for category Count*/
		$earning = $this->CRUD_model->getAdminAuthorEarning();
		$data['earning'] = $earning;
		$data['settinglist'] = get_setting();

		$this->load->view("admin/dashboard/dashboard", $data);

	}
}
