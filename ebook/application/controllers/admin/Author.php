<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author extends CI_Controller{

	public function __construct() {
		parent::__construct();
		$CI =& get_instance();
		frontendcheck();
	}

	public function index(){
		$where = 'role_id=2';
		$data['authorlist'] = $this->CRUD_model->get($where,'author','id','desc');
		$this->load->view("admin/author/list",$data);
	}

	public function add(){
		$this->load->view("admin/author/add");
	}

	public function save(){
		$this->form_validation->set_rules('name', 'Author Name', 'required');
        $this->form_validation->set_rules('bio', 'Bio ', 'required');
        $this->form_validation->set_rules('email', 'Email ', 'required');
        $this->form_validation->set_rules('password', 'Password ', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $errors = $this->form_validation->error_array();
            sort($errors);
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
			{ 
				$author_image = $this->CRUD_model->imageupload($_FILES['image'],'image', FCPATH . 'assets/images/author');
			}else
			{
				$res=array('status'=>'400','message'=>array('Please select author image.'));
				echo json_encode($res);exit;	
			}

			$bio = '';
			if(isset($_POST['bio']) && $_POST['bio'] != '')
			{
				$bio = $_POST['bio'];
			}else
			{
				$res=array('status'=>'400','message'=>array('Please select author image.'));
				echo json_encode($res);exit;		
			}



			$data = array(
				'name' => $_POST['name'],
				'image' => $author_image,
				'role_id' => '2',
				'bio' => $_POST['bio'],
				'email' => $_POST['email'],
				'password' => md5($_POST['password']),
				'address' => $_POST['address']
			);

			$id=$this->CRUD_model->insert($data,'author');
			$res=array('status'=>'200','message'=>'Sucessfully updated','id'=>$id);
			echo json_encode($res);exit;
		}
	}

	public function edit(){
		$where='id="'.$_GET['id'].'"';
		$data['author'] = $this->CRUD_model->getById($where,'author');
		$this->load->view("admin/author/edit",$data);
	}

	public function update(){
		$this->form_validation->set_rules('name', 'Author Name', 'required');
        $this->form_validation->set_rules('bio', 'Bio ', 'required');
        $this->form_validation->set_rules('email', 'Email ', 'required');
        $this->form_validation->set_rules('password', 'Password ', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        if ($this->form_validation->run() == FALSE)
        {
             $errors = $this->form_validation->error_array();
            sort($errors); 	
            $array  = array('status'=>400,'message'=>$errors);
         	echo json_encode($array);exit;
        }
        else
        {
			$name=$_POST['name'];
			if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
				$author_image=$this->CRUD_model->imageupload($_FILES['image'],'image', FCPATH . 'assets/images/author');
				
			}else{
				$author_image=$_POST['authorimage'];	
			}

			$data = array(
				'name' => $name,
				'image' => $author_image,
				'bio' => $_POST['bio'],
				'email' => $_POST['email'],
				'password' => md5($_POST['password']),

				'address' => $_POST['address']
			);
			
			$cat_id=$this->CRUD_model->update($_POST['id'],'id',$data,'author');	

			$res=array('status'=>'200','message'=>'Sucessfully updated','id'=>$cat_id);
			echo json_encode($res);exit;
		}
	}

	public function fetch_data(){  

    	$table = "author";
	  	$select_column = array("id","image","name","email", "bio");  
	  	$order_column = array(null, "name", "bio");  
	  	$search = array('name','bio');
	  	$where = 'role_id=2';
       	$fetch_data = $this->CRUD_model->make_datatables($table, $select_column,$order_column,$search,$where);  
       	$data = array();  
       	foreach($fetch_data as $key=> $row)  
       	{  
            $sub_array = array();  
           	$sub_array[] = '<img src="'.base_url().'assets/images/author/'.$row->image.'"  width="50" height="35" />';  
            $sub_array[] = string_cut($row->name,15);
            $sub_array[] = string_cut($row->email,15);
            $sub_array[] = string_cut($row->bio,15);  
            
            $sub_array[] = '<a href="'.base_url().'admin/author/edit?id='.$row->id.'">Edit</a> | <a href="javaScript:void(0)" onclick="delete_record('.$row->id.',\'author\')">Delete</a>';
            $data[] = $sub_array;  
       	}
       	$output = array(  
            "draw"             => intval($_POST["draw"]),  
            "recordsTotal"     => $this->CRUD_model->get_all_data($table),  
            "recordsFiltered"  => $this->CRUD_model->get_filtered_data($table,$select_column,$order_column,$search=null),  
            "data"             => $data  
       	);  
       	echo json_encode($output);  
    }
}
