<?php
  $this->load->view('admin/comman/header');
  $setn=array(); 
  foreach($settinglist as $set){
    $setn[$set->key]=$set->value;
  }
?> 

<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!--Start Dashboard Content-->
      <div class="row mt-4">
        <div class="col-12 col-lg-6 col-xl-3">
        <div class="card gradient-knight">
          <div class="card-body">
            <div class="media">
              <span class="text-white" style="font-size:30px;"><i class="fa fa-home"></i></span>
              <div class="media-body text-left" style="margin-left: 10px">
                  <h4 class="text-white"><?php echo $wallpaper;?></h4>
                  <h5 class="text-white">Books</<h5>
                </div>
                <div class="align-self-center"><span id="dash-chart-1"></span></div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-scooter">
            <div class="card-body">
              <div class="media">
                <span class="text-white" style="font-size:30px;"><i class="fa fa-book"></i></span>
               <div class="media-body text-left" style="margin-left: 10px">
                  <h4 class="text-white"><?php echo $category;?></h4>
                  <h5 class="text-white">Category</h5>
                </div>
                <div class="align-self-center"><span id="dash-chart-3"></span></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-quepal">
            <div class="card-body">
              <div class="media">
                 <span class="text-white" style="font-size:30px;"><i class="fa fa-user"></i></span>
                <div class="media-body text-left" style="margin-left: 10px">
                    <h4 class="text-white"><?php echo $register_user;?></h4>
                    <h5 class="text-white">Active User</h5>
                  </div>
                  <div class="align-self-center"><span id="dash-chart-3"></span></div>
                </div>
              </div>
            </div>
          </div>
      </div><!--End Row-->

      <div class="row mt-4">
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-purpink">
            <div class="card-body">
              <div class="media">
                 <span class="text-white" style="font-size:30px;"><i class="fa fa-users"></i></span>
                <div class="media-body text-left" style="margin-left: 10px">
                    <h4 class="text-white"><?php echo $autohr;?></h4>
                    <h5 class="text-white">Autohrs</h5>
                  </div>
                  <div class="align-self-center"><span id="dash-chart-3"></span></div>
                </div>
              </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-redmist">
            <div class="card-body">
              <div class="media">
                <span class="text-white" style="font-size:30px;"><i class="fa fa-money"></i></span>
                <div class="media-body text-left" style="margin-left: 10px">
                  <h4 class="text-white"><?php echo $_SESSION['currency_symbol'];?><?php echo isset($earning->total_commission) ? $earning->total_commission : '0';?></h4>
                  <h5 class="text-white">Commissions</h5>
                </div>
                <div class="align-self-center"><span id="dash-chart-3"></span></div>
              </div>
            </div>
          </div>
        </div>
     
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-steelgray">
            <div class="card-body">
              <div class="media">
                <span class="text-white" style="font-size:30px;"><i class="fa fa-money"></i></span>
                <div class="media-body text-left" style="margin-left: 10px">
                  <h4 class="text-white"><?php echo $_SESSION['currency_symbol'];?><?php echo isset($earning->total_earning) ? $earning->total_earning : '0';?></h4>
                  <h5 class="text-white">Earnings</h5>
                </div>
                <div class="align-self-center"><span id="dash-chart-3"></span></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
    
    <?php $this->load->view('admin/comman/footerpage');?>

