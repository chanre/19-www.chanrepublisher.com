<?php 
    $setn=array(); 
    $settinglist = get_setting(); 

    foreach($settinglist as $set){
        $setn[$set->key]=$set->value;
    }
    $data['setn'] = $setn;
?> 
<!-- Start wrapper-->
<div id="wrapper">
  <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo">
        <a href="<?php echo base_url();?>admin">
            <img src="<?php echo base_url().'assets/images/app/'.$setn['app_logo']; ?>" class="logo-icon" >
            <h5 class="logo-text"><?php echo $setn['app_name'];?></h5>
        </a>
    </div>
    <div class="" style="max-height: 550px;min-height: 550px;overflow-y: auto;">
    <ul class="sidebar-menu do-nicescrol">
      <?php if($_SESSION['role_id'] == 1){?>
      <li>
        <a href="<?php echo base_url();?>admin/dashboard" class="waves-effect">
            <i class="icon-home"></i><span>Dashboard</span></i>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/category" class="waves-effect">
          <i class="fa fa-list"></i><span>Category</span>
        </a>
      </li>
      <li><a href="<?php echo base_url();?>admin/adsbanner" class="waves-effect"><i class="fa fa-image"></i><span>Ads Banner</span></a></li>
      
      <li>
        <a href="<?php echo base_url();?>admin/users" class="waves-effect">
          <i class="fa fa-user"></i><span>User</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/author" class="waves-effect">
          <i class="fa fa-edit"></i><span>Author</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/book" class="waves-effect">
          <i class="fa fa-book"></i><span>Books</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/notification" class="waves-effect">
          <i class="fa fa-bell"></i><span>Notification</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/comments" class="waves-effect">
          <i class="fa fa-exchange"></i><span>Comments</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/report/sales_report" class="waves-effect">
          <i class="fa fa-line-chart"></i><span>Sales Report</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/report/settlement" class="waves-effect">
          <i class="fa fa-bank"></i><span>Settlement</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/report/payout" class="waves-effect">
          <i class="fa fa-money"></i><span>Payout History</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/setting" class="waves-effect">
          <i class="fa fa-cogs"></i><span>Setting</span>
        </a>
      </li>
    <?php }else{?>
      <li>
        <a href="<?php echo base_url();?>admin/Authorprofile/dashboard" class="waves-effect">
          <i class="icon-home"></i><span>Dashboard</span></i>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/book" class="waves-effect">
          <i class="fa fa-book"></i><span>Books</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/Authorprofile/payout" class="waves-effect">
          <i class="fa fa-exchange"></i><span>Payout Report</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/Authorprofile/sales_report" class="waves-effect">
          <i class="fa fa-line-chart"></i><span>Sales Report</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url();?>admin/Authorprofile/profile" class="waves-effect">
          <i class="fa fa-user"></i><span> Profile </span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url();?>admin/Authorprofile/bankdetail" class="waves-effect">
          <i class="fa fa-bank"></i><span> Bankdetail </span>
        </a>
      </li>
      
    <?php } ?>
    <li>
      <a href="<?php echo base_url();?>admin/Login/logout" class="waves-effect">
        <i class="icon-power mr-2"></i><span>Logout</span>
      </a>
    </li>
  </ul>
  </div>
</div>
<!--End sidebar-wrapper-->