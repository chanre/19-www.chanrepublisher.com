<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
    $setn=array(); 
    $settinglist = get_setting(); 

    foreach($settinglist as $set){
        $setn[$set->key]=$set->value;
    }
    $data['setn'] = $setn;
?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title><?php echo $setn['app_name'];?></title>

    <?php header('Access-Control-Allow-Origin: *'); ?>

    <!--favicon-->
    <link rel="icon" href="<?php echo base_url().'assets/images/app/'.$setn['app_logo']; ?>" type="image/x-icon">
    <link href="<?php echo base_url();?>assets/js/select2/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap core CSS-->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>assets/js/datatable/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link  href="<?php echo base_url();?>assets/plugins/jquery-datepicker/css/jquery-ui.css" rel="stylesheet">
    <!-- animate CSS-->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <!-- Sidebar CSS-->
    <link href="<?php echo base_url();?>assets/css/sidebar-menu.css" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="<?php echo base_url();?>assets/css/app-style.css" rel="stylesheet"/>

    <link href="<?php echo base_url();?>assets/plugins/sweetalert/css/sweetalert.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/toastr.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" /> 
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/jquery-datepicker/js/jquery-ui.js"></script>

    <link href="<?php echo base_url();?>assets/plugins/summernote/dist/summernote-bs3.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>assets/plugins/summernote/dist/summernote.css" rel="stylesheet"/>
    <script src="<?php echo base_url();?>assets/js/select2/select2.min.js" defer></script>
</head>
<?php $this->load->view('admin/comman/sidemenu');?>

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top bg-white">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
    
  </ul>
     
  <ul class="navbar-nav align-items-center right-nav-link">
    
    <li class="nav-item">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
        <span class="user-profile"><img src="<?php echo base_url();?>assets/images/author/<?php echo $this->session->userdata('image');?>" class="img-circle" alt="user avatar"></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-right animated fadeIn">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
             <div class="avatar"><img class="align-self-start mr-3" src="<?php echo base_url();?>assets/images/author/<?php echo $this->session->userdata('image');?>" alt="user avatar"></div>
            <div class="media-body">
            <h6 class="mt-2 user-title"><?php echo $this->session->userdata('name');?></h6>
            <p class="user-subtitle"><?php echo $this->session->userdata('email');?></p>
            </div>
           </div>
          </a>
        </li>
        <li class="dropdown-divider"></li>
        <li class="dropdown-item"><a href="<?php echo base_url();?>admin/Login/logout"> <i class="icon-power mr-2"></i>Logout</a></li>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End topbar header-->
