<?php
$this->load->view('admin/comman/header');
?>

<div class="clearfix"></div>

<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add Author</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
          			<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/author">Authors</a></li>
					<li class="breadcrumb-item active" aria-current="page">Add Author</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/authorlist" class="btn btn-outline-primary waves-effect waves-light">Authors List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Add Author
							<form id="edit_video_form"  enctype="multipart/form-data">

								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
										<label for="name">Author Name</label>
										<input type="text" required value="" class="form-control" name="name" id="name" >
									</div>
									<div class="col-sm-6 ">
										<label for="address">Author Address</label>
										<input type="text" required value="" class="form-control" name="address" id="address" >
									</div>
								</div>
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
										<label for="name"> Email </label>
										<input type="email" required value="" class="form-control" name="email" id="email" >
									</div>
									<div class="col-sm-6 ">
										<label for="address"> Password </label>
										<input type="password" required value="" class="form-control" name="password" id="password" >
									</div>
								</div>
								<div class="form-row mt-3">
                            	    <div class="col-sm-12 ">
										<label for="input-1">Book Description</label>
										<textarea  class="form-control summernote" name="bio" id="bio"></textarea>
									</div>
								</div>
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
										<label for="input-1"> Author Profile Picture</label>
										<input type="file" required  class="form-control" name="image" id="image" onchange="readURL(this,'showImage')">
										<p class="noteMsg">Note: Image Size must be less than 2MB.Image Height and Width less than 1000px.</p>
										<img id="showImage" src="<?php echo base_url().'assets/images/placeholder.png';?>" height="150" width="150" alt="your image" />
									</div>
								</div>
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
										<button type="button" onclick="saveauthor()" class="btn btn-primary shadow-primary px-5">Save</button>
									</div>
								</div>	

							</form>
						</div>
					</div>


				</div></div></div>
			</div>

			<?php
			$this->load->view('admin/comman/footerpage');
			?>
			<script type="text/javascript">

				function saveauthor(){

					displayLoader();
					var formData = new FormData($("#edit_video_form")[0]);

					var textareaValue = $('#bio').code();
					formData.append("bio", textareaValue);

					$.ajax({
						type:'POST',
						url:'<?php echo base_url(); ?>admin/author/save',
						data:formData,
						cache:false,
						contentType: false,
						processData: false,
						dataType: "json",
						success:function(resp){
							hideLoader();
							if(resp.status=='200'){
								document.getElementById("edit_video_form").reset();
								toastr.success(resp.message,'success');
								setTimeout(function(){ 
									window.location.replace('<?php echo base_url(); ?>admin/author');
								}, 500);
							}else{
								var obj = resp.message;
								$.each(obj, function(i,e) {
							        toastr.error(e);
							    });
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							hideLoader();
							toastr.error(errorThrown.msg,'failed');         
						}
					});
				}
			</script>