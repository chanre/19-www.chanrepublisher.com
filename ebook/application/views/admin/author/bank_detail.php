<?php $this->load->view('admin/comman/header');?>

<div class="clearfix"></div>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add Bank detail</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/author">Bank detail</a></li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/author" class="btn btn-outline-primary waves-effect waves-light"></a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Bank Detail
							<form id="edit_update_bankdetail"  enctype="multipart/form-data">

								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
                            	   		<label for="name">Bank Name</label>
										<input type="text" value="<?php echo isset($bankdetail->bank_name) ? $bankdetail->bank_name : '';?>" class="form-control" name="bank_name" id="bank_name" placeholder="Enter Bank Name">
									</div>
									<div class="col-sm-6 ">
                            			<label for="Address">Account No</label>
										<input type="text" value="<?php echo isset($bankdetail->account_no) ? $bankdetail->account_no : ''; ?>" class="form-control" name="account_no" id="account_no" >
									</div>
								</div>
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
                            	   		<label for="name">IFSC Code</label>
										<input type="text" value="<?php echo isset($bankdetail->ifsc_code) ? $bankdetail->ifsc_code : '';?>" class="form-control" name="ifsc_code" id="ifsc_code" placeholder="Enter IFSC Code">
									</div>
									<div class="col-sm-6 ">
                            			<label for="Address">Holder Name</label>
										<input type="text" value="<?php echo  isset($bankdetail->bank_holder_name) ? $bankdetail->bank_holder_name : ''; ?>" class="form-control" name="bank_holder_name" id="bank_holder_name" >
									</div>
								</div>
									
								<input type="hidden" name="id" value="<?php echo isset($bankdetail->id) ? $bankdetail->id :''; ?>">
								
								<div class="form-row mt-3">
                            	    <div class="col-sm-12 ">
	                            		<button type="button" onclick="updateauthor()" class="btn btn-primary shadow-primary px-5">Save</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div></div></div>
			</div>

			<?php
			$this->load->view('admin/comman/footerpage');
			?>
			<script type="text/javascript">

				function updateauthor(){

					displayLoader();
					var formData = new FormData($("#edit_update_bankdetail")[0]);

					$.ajax({
						type:'POST',
						url:'<?php echo base_url(); ?>admin/Authorprofile/update_bankdetail',
						data:formData,
						cache:false,
						contentType: false,
						processData: false,
						dataType: "json",
						success:function(resp){
							hideLoader();
							if(resp.status=='200'){
								document.getElementById("edit_authoreo_form").reset();
								toastr.success(resp.message,'success');
								setTimeout(function(){ 
									window.location.replace('<?php echo base_url(); ?>admin/Authorprofile/profile');
								}, 500);
							}else{
								var obj = resp.message;
								$.each(obj, function(i,e) {
							        toastr.error(e);
							    });
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							hideLoader();
							toastr.error(errorThrown.msg,'failed');         
						}
					});
				}
			</script>