<?php
  $this->load->view('admin/comman/header');

  $setn=array(); 
  $settinglist = get_setting(); 
  foreach($settinglist as $set){
   $setn[$set->key]=$set->value;
  }

?>
<?php 
  $total_earning  = isset($dashboard->total_earning) ? $dashboard->total_earning : '0';
  $total_commission = isset($dashboard->total_commission) ? $dashboard->total_commission : '0'; 
  $earning =  $total_earning - $total_commission;
?>
<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!--Start Dashboard Content-->
    <div class="row mt-4">
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-scooter">
            <div class="card-body">
              <div class="media">
                <span class="text-white" style="font-size:30px;"><i class="fa fa-book"></i></span>
               <div class="media-body text-left" style="margin-left: 10px">
                  <h4 class="text-white"><?php echo $_SESSION['currency_symbol'];?><?php echo $earning;?></h4>
                  <h5 class="text-white"> Earnings </h5>
                </div>
                <div class="align-self-center"><span id="dash-chart-3"></span></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-quepal">
            <div class="card-body">
              <div class="media">
                 <span class="text-white" style="font-size:30px;"><i class="fa fa-user"></i></span>
                <div class="media-body text-left" style="margin-left: 10px">
                   <h4 class="text-white"><?php echo $_SESSION['currency_symbol'];?><?php echo isset($dashboard->total_commission) ? $dashboard->total_commission : '0'; ?></h4>
                    <h5 class="text-white"> Commissions </<h5>
                  </div>
                  <div class="align-self-center"><span id="dash-chart-3"></span></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-lg-6 col-xl-3">
          <div class="card gradient-cherry">
            <div class="card-body">
              <div class="media">
                 <span class="text-white" style="font-size:30px;"><i class="fa fa-user"></i></span>
                <div class="media-body text-left" style="margin-left: 10px">
                   <h4 class="text-white"><?php echo isset($book) ? $book : '0'; ?></h4>
                    <h5 class="text-white"> Books </<h5>
                  </div>
                  <div class="align-self-center"><span id="dash-chart-3"></span></div>
                </div>
              </div>
            </div>
          </div>
      </div><!--End Row-->
    </div><!--End content-wrapper-->

<?php  $this->load->view('admin/comman/footerpage'); ?>

