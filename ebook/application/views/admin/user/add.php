<?php $this->load->view('admin/comman/header');?>

<div class="clearfix"></div>

<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add User</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/users">Users</a></li>
					<li class="breadcrumb-item active" aria-current="page">Add User</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/userlist" class="btn btn-outline-primary waves-effect waves-light">UserList</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Add User
							<form id="user_form"  enctype="multipart/form-data">
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
	                            		<label for="fullname">FullName</label>
										<input type="text" required value="" class="form-control" name="fullname" id="fullname" >
									</div>
									<div class="col-sm-6 ">
		                            	<label for="email">Email</label>
										<input type="text" required value="" class="form-control" name="email" id="email" >
									</div>
								</div>		
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
	                            		<label for="mobile">Mobile Number</label>
										<input type="Number" required value="" class="form-control" name="mobile" id="mobile" >
									</div>
									<div class="col-sm-6 ">
	                            		<label for="password">Password</label>
										<input type="Password" required value="" class="form-control" name="password" id="password" >
									</div>
								</div>
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
	                            		<button type="button" onclick="saveuser()" class="btn btn-primary shadow-primary px-5">Save</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php	$this->load->view('admin/comman/footerpage'); ?>
<script type="text/javascript">
	function saveuser(){
		displayLoader();
		var formData = new FormData($("#user_form")[0]);
		$.ajax({
			type:'POST',
			url:'<?php echo base_url(); ?>admin/users/save',
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: "json",
			success:function(resp){
				hideLoader();
				if(resp.status=='200'){
					document.getElementById("user_form").reset();
					toastr.success(resp.message);
					window.location.replace('<?php echo base_url(); ?>admin/users');
				}else{
					var obj = resp.message;
					$.each(obj, function(i,e) {
				        toastr.error(e);
				    });
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				hideLoader();
				toastr.error(errorThrown.message,'failed');         
			}
		});
	}
</script>