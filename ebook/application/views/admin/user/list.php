<?php $this->load->view('admin/comman/header');?>
<!-- UserList Data Show -->
<div class="clearfix"></div>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">User List</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/users">Users</a></li>
					<li class="breadcrumb-item active" aria-current="page">User List</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/users/add" class="btn btn-outline-primary waves-effect waves-light">Add User</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<div class="row">
			<div class="col-lg-12">
			<div class="card">
				<div class="card-header"> User List</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="user-datatable" class="table table-bordered">
							<thead>
								<tr>
									<th>FullName</th>
									<th>Email</th>
									<th>Mobile Number</th>
									<th>Type</th>
									<th>Created Date</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- End Row-->


	<?php $this->load->view('admin/comman/footerpage'); ?>
<script>
$(document).ready(function(){  
    var dataTable = $('#user-datatable').DataTable({  
      "processing":true,  
      "serverSide":true,  
      "order":[],  
      "ajax":{  
        url:"<?php echo base_url().'admin/users/fetch_data'; ?>",  
        type:"POST"  
      },  
      "columnDefs":[  
        {  
          //  "targets":[0, 3, 4],  
          "orderable":false,  
        },  
      ],  
    });  
});  
  </script>