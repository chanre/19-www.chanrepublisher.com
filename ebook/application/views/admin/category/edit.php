<?php $this->load->view('admin/comman/header'); ?>
 <div class="clearfix"></div>

    <div class="content-wrapper">
        <div class="container-fluid">
           <div class="col p-md-0">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/category">Category</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Update Category</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Update Category</h4>

                            <form id="edit_category_form"  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="input-1">Category Name</label>
                                    <input type="text" name="name" class="form-control col-8" id="name" placeholder="Enter Your Category Name" value="<?php echo $category->name?>">
                                </div>
        
                                <input type="hidden" name="id" value="<?PHP echo $category->id; ?>">

                                <div class="form-group">
                                    <label for="input-2">Category Image</label>
                                    <input type="file" name="image" class="form-control col-8" id="image" onchange="readURL(this,'categoryDisImage')" >
                                    <input type="hidden" name="categoryimage" value="<?PHP echo $category->image; ?>" >
                                     <p class="noteMsg">Note: Image Size must be lessthan 2MB.Image Height and Width less than 1000px.</p>
                                    <div class="imgageResponsive">
                                        <img id="categoryDisImage" src="<?php echo base_url().'assets/images/category/'.$category->image; ?>" height="auto;" width="150px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="button" onclick="updateCategory()" class="btn btn-primary shadow-primary px-5">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
    </div>

<script type="text/javascript">
    function updateCategory(){
        var category_name=jQuery('input[name=name]').val();
        if(category_name==''){
            toastr.error('Please enter category name','failed');
            return false;
        }
        $("#dvloader").css('display','block');
        $("body").addClass('overlay');

        var formData = new FormData($("#edit_category_form")[0]);
        $.ajax({
            type:'POST',
            url:'<?php echo base_url(); ?>admin/category/update',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            dataType:'json',
            success:function(resp){
             if(resp.status=='200'){
                 $("#dvloader").css('display','none');
                $("body").removeClass('overlay');
                toastr.success(resp.message);
                window.location.replace('<?php echo base_url(); ?>admin/category');
                }else{
                    toastr.error(resp.message);
                    $("#dvloader").hide();
                }
            }
        });
    }
</script>
       
<?php
   $this->load->view('admin/comman/footerpage');
  ?>