<?php $this->load->view('admin/comman/header');?>

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">

   <div class="row pt-2 pb-2">
      <div class="col p-md-0">
         <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/category">Category</a></li>
          <li class="breadcrumb-item active"><a href="javascript:void(0)">Add Category</a></li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
  <div class="row">
     <div class="col-lg-12">
        <div class="card">
           <div class="card-body">
              <div class="card-title">Add Category</div>
              <hr>
              <form id="category_form"  enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input-1">Category Name</label>
                  <input type="text" name="name" class="form-control col-8" id="name" placeholder="Enter Your Category Name">
                </div>
                <div class="form-group">
                    <label for="input-2">Category Image</label>
                    <input type="file" name="image" class="form-control col-8" id="input-2" onchange="readURL(this,'showImage')" >
                    <p class="noteMsg">Note: Image Size must be lessthan 2MB.Image Height and Width less than 1000px.</p>
                    <img id="showImage" src="<?php echo base_url().'assets/images/placeholder.png';?>" height="150" width="150" alt="your image" />
                </div>
                <div class="form-group">
                  <button type="button" onclick="saveCategory()" class="btn btn-primary shadow-primary px-5">Save</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
</div>
</div>

<script type="text/javascript">
  function saveCategory(){
    var formData = new FormData($("#category_form")[0]);
     displayLoader();
     $.ajax({
        type:'POST',
        url:'<?php echo base_url(); ?>admin/category/save',
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        dataType:'json',
        success:function(resp){
          hideLoader();
          if(resp.status=='200'){
            document.getElementById("category_form").reset();
            toastr.success(resp.message);
            window.location.replace('<?php echo base_url(); ?>admin/category');
          }else{
            toastr.error(resp.message);
          }
        }
      });
    }
</script>

<?php $this->load->view('admin/comman/footerpage'); ?>