<?php $this->load->view('admin/comman/header');?>
<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Category list</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/category">Category</a></li>
          <li class="breadcrumb-item active" aria-current="page">Category list</li>
        </ol>
      </div>
      <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="<?php echo base_url();?>admin/category/add" class="btn btn-outline-primary waves-effect waves-light">Add Category</a>
      </div>
    </div>
  </div>
  <!-- End Breadcrumb-->
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header"><i class="fa fa-table"></i> Category</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="category-datatable" class="table table-bordered">
             <thead>
              <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Created date</th>
                <th>Action</th>
              </tr>
            </thead>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<?php  $this->load->view('admin/comman/footerpage');?>

<script>
  $(document).ready(function(){  
    var dataTable = $('#category-datatable').DataTable({  
      "processing":true,  
      "serverSide":true,  
      "order":[],  
      "ajax":{  
        url:"<?php echo base_url().'admin/category/fetch_data'; ?>",  
        type:"POST"  
      },  
      "columnDefs":[  
        {  
          //  "targets":[0, 3, 4],  
          "orderable":false,  
        },  
      ],  
    });  
  });  
  </script>