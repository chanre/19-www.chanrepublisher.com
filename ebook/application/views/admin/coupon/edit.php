<?php
$this->load->view('admin/comman/header');
?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" defer></script>


<div class="clearfix"></div>



<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add Coupon</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javaScript:void();">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javaScript:void();">Coupon</a></li>
					<li class="breadcrumb-item active" aria-current="page">Update Coupon</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/couponlist" class="btn btn-outline-primary waves-effect waves-light">Coupon List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<?php // print_r($coupon);?>
		<div class="row">
			<div class="col-lg-10 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Update coupon</div>
						<hr>
						<form id="edit_coupon_form"  enctype="multipart/form-data">
							<div class="form-group">
								<label for="input-1">Coupon Code</label>
								<input type="text" name="coupon_code" value="<?PHP echo $coupon[0]->coupon_code; ?>" class="form-control" id="coupon_code">
							</div>

							<div class="form-group">
								<label for="input-1">Amount</label>
								<input type="text" name="amount" value="<?PHP echo $coupon[0]->amount; ?>" class="form-control" id="amount">
							</div>
							<div class="form-group">
								<label for="input-1">Start date</label>
								<input type="text" name="start_date" value="<?PHP echo $coupon[0]->start_date; ?>" class="form-control" id="start_date">
							</div>
							<div class="form-group">
								<label for="input-1">End Date</label>
								<input type="text" name="end_date" value="<?PHP echo $coupon[0]->end_date;?>" class="form-control" id="end_date">
							</div>
							<input type="hidden" name="id" value="<?PHP echo $coupon[0]->id; ?>">

							<div class="form-group">
								<button type="button" onclick="updatecoupon()" class="btn btn-primary shadow-primary px-5">Update</button>
							</div>
						</form>
					</div>
				</div>


			</div></div></div>
		</div>
		<?php
		$this->load->view('admin/comman/footerpage');
		?>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('#start_date').datepicker({
				    format: 'yyyy-mm-dd',
				});
				$('#end_date').datepicker({
				    format: 'yyyy-mm-dd',
				});
			});		function updatecoupon(){
				var coupon_name=jQuery('input[name=coupon_code]').val();
				if(coupon_name==''){
					toastr.error('Please enter coupon code','failed');
					return false;
				}
				$("#dvloader").show();

				var formData = new FormData($("#edit_coupon_form")[0]);
				$.ajax({
					type:'POST',
					url:'<?php echo base_url(); ?>admin/updatecoupon',
					data:formData,
					cache:false,
					contentType: false,
					processData: false,
					dataType: 'json', 
					success:function(resp){
						$("#dvloader").hide();
						if(resp.status=='200'){
							document.getElementById("edit_coupon_form").reset();
							toastr.success(resp.msg);
							window.location.replace('<?php echo base_url(); ?>admin/couponlist');
						}else{
							toastr.error(resp.msg);
						}
					}
				});
			}
		</script>