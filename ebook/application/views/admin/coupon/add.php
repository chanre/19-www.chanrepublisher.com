<?php
	$this->load->view('admin/comman/header');
	 $setn=array(); 
	foreach($settinglist as $set){
		$setn[$set->key]=$set->value;
	}
?> 

<div class="clearfix"></div>

<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add Coupon</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javaScript:void();">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="javaScript:void();">Coupon</a></li>
					<li class="breadcrumb-item active" aria-current="page">Add Coupon</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/coupon_list" class="btn btn-outline-primary waves-effect waves-light">Coupon List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->

		<div class="row">
			<div class="col-lg-10 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Add coupon</div>
						<hr>
						<form id="coupon_form"  enctype="multipart/form-data">
							<div class="form-group">
								<label for="input-1">Coupon code</label>
								<input type="text" name="coupon_code" class="form-control" id="coupon_code">
							</div>
							<div class="form-group">
								<label for="input-1">Amount</label>
								<input type="text" name="amount" class="form-control" id="amount">
							</div>
							<div class="form-group">
								<label for="start_date">Start Date</label>
								<input type="text" name="start_date" class="form-control start_date">
							</div>
							<div class="form-group">
								<label for="input-1">End Date</label>
								<input type="text" name="end_date" class="form-control"  id="end_date">
							</div>
												<div class="form-group">
								<button type="button" onclick="savecoupon()" class="btn btn-primary shadow-primary px-5">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div></div></div>
		</div>

		<?php
		$this->load->view('admin/comman/footerpage');
		?>	
		
		<script type="text/javascript">
			$.noConflict();  //Not to conflict with other scripts
			jQuery(document).ready(function($) {

			$( ".start_date" ).datepicker();

			});
		
			function savecoupon(){

				var formData = new FormData($("#coupon_form")[0]);
				var coupon_name = $("#coupon_name").val();
				var price = $("#price").val();
				if(coupon_name == '') {
					toastr.error('Please enter coupon code.');
					$("#coupon").focus();
					return false;
				}
				
				$("#dvloader").show();
				$.ajax({
					type:'POST',
					url:'<?php echo base_url(); ?>admin/savecoupon',
					data:formData,
					cache:false,
					contentType: false,
					processData: false,
					dataType: 'json', 
					success:function(resp){
						$("#dvloader").hide();
						if(resp.status=='200'){
							document.getElementById("coupon_form").reset();
							toastr.success(resp.msg);
							window.location.replace('<?php echo base_url(); ?>admin/couponlist');
						}else{
							toastr.error(resp.msg);
						}
					}
				});
			}
		</script>