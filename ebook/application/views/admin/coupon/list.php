<?php
 $this->load->view('admin/comman/header');
?>

<link href="<?php echo base_url();?>assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Coupon List</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Coupon</a></li>
            <li class="breadcrumb-item active" aria-current="page">Coupon List</li>
         </ol>
	   </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
           <a href="<?php echo base_url();?>admin/addcoupon" class="btn btn-outline-primary waves-effect waves-light">Add Coupon</a>
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> coupon Record</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Code</th>
                        <th>Amount</th>
                        <th>start Date</th>
                        <th>End Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                     <?php $i=1;foreach($coupons as $coupon){ ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $coupon->coupon_code; ?></td>
                      <td><?php echo $coupon->amount; ?></td>
                      <td><?php echo $coupon->start_date; ?></td>
                      <td><?php echo $coupon->end_date; ?></td>
                      <td><a href="<?php echo base_url()?>admin/editcoupon?id=<?php echo $coupon->id; ?>">Edit</a> | <a href="javaScript:void(0)" onclick="delete_record('<?php echo $coupon->id; ?>','coupon')">Delete</a></td>
                    </tr>
                  <?php $i++;} ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

<?php
  $this->load->view('admin/comman/footerpage');
?>
 <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();
      } );

    </script>
