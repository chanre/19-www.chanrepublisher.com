<?php $this->load->view('admin/comman/header'); ?>

<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Ads Banner list</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/adsbanner">Ads Banner</a></li>
          <li class="breadcrumb-item active" aria-current="page">Ads banner list</li>
        </ol>
      </div>
      <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="<?php echo base_url();?>admin/adsbanner/add" class="btn btn-outline-primary waves-effect waves-light">Add Ads Banner</a>

      </div>
    </div>
  </div>
  <!-- End Breadcrumb-->
  <div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-header"> Ads Banner Record</div>
            <div class="card-body">
            <div class="table-responsive">
                <table id="ads_banner-datatable" class="table table-bordered" >
                  <thead class="card-header-tabs">
                    <tr>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                </table>
            </div>
      </div>
    </div>            
  </div>
</div><!-- End Row-->

<?php
$this->load->view('admin/comman/footerpage');
?>

<script>
  $(document).ready(function(){  
    var dataTable = $('#ads_banner-datatable').DataTable({  
      "processing":true,  
      "serverSide":true,  
      "order":[],  
      "ajax":{  
        url:"<?php echo base_url() . 'admin/adsbanner/fetch_data'; ?>",  
        type:"POST"  
      },  
      "columnDefs":[  
        {  
          //  "targets":[0, 3, 4],  
          "orderable":false,  
        },  
      ],  
    });  
  });  
</script>