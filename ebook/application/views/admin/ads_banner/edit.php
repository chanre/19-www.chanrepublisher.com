<?php $this->load->view('admin/comman/header');?>

<div class="clearfix"></div>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add Ads Banner</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/adsbanner">Ads Banner</a></li>
					<li class="breadcrumb-item active" aria-current="page">Add Ads Banner</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/adsbanner" class="btn btn-outline-primary waves-effect waves-light">Ads Banner List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Add Ads Banner
							<form id="edit_ads_bannereo_form"  enctype="multipart/form-data">

								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
                            	   		<label for="name"> Name</label>
										<input type="text" value="<?php echo $ads_banner->name;?>" class="form-control" name="name" id="name" placeholder="Enter Book Name">
									</div>
								</div>
								<input type="hidden" name="id" value="<?php echo $ads_banner->id; ?>">
								
								<div class="form-row mt-3">
                            	    <div class="col-sm-6 ">
	                            		<label for="input-1"> Banner </label>
										<input type="file" required  class="form-control" name="image" id="image" onchange="readURL(this,'showImage')">
										<input type="hidden" name="ads_bannerimage" value="<?php echo $ads_banner->image; ?>">
										<p>Note: Image Size must be less than 2MB.Image Height and Width less than 1000px.</p>
										<div><img id="showImage" src="<?php echo base_url().'assets/images/banner/'.$ads_banner->image; ?>" height="100px;" width="250px;"></div>
									</div>
								</div>
								<div class="form-row mt-3">
                            	    <div class="col-sm-12 ">
	                            		<button type="button" onclick="updateads_banner()" class="btn btn-primary shadow-primary px-5">Save</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div></div></div>
			</div>

			<?php
			$this->load->view('admin/comman/footerpage');
			?>
			<script type="text/javascript">

				function updateads_banner(){

					displayLoader();
					var formData = new FormData($("#edit_ads_bannereo_form")[0]);

					$.ajax({
						type:'POST',
						url:'<?php echo base_url(); ?>admin/adsbanner/update',
						data:formData,
						cache:false,
						contentType: false,
						processData: false,
						dataType: "json",
						success:function(resp){
							hideLoader();
							if(resp.status=='200'){
								document.getElementById("edit_ads_bannereo_form").reset();
								toastr.success(resp.message,'success');
								setTimeout(function(){ 
									window.location.replace('<?php echo base_url(); ?>admin/adsbanner');
								}, 500);
							}else{
								var obj = resp.message;
								$.each(obj, function(i,e) {
							        toastr.error(e);
							    });
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							hideLoader();
							toastr.error(errorThrown.msg,'failed');         
						}
					});
				}
			</script>