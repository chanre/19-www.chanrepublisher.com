<?php
$this->load->view('admin/comman/header');
?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<div class="clearfix"></div>

<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add package</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
            		<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/package">Package</a></li>
					<li class="breadcrumb-item active" aria-current="page">Update package</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/package/list" class="btn btn-outline-primary waves-effect waves-light">package List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<?php // print_r($package);?>
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Update package</div>
						<hr>
						<form id="edit_package_form"  enctype="multipart/form-data">
							<div class="form-group">
								<label for="input-1">package Name</label>
								<input type="text" name="package_name" value="<?PHP echo $package->package_name; ?>" class="form-control" id="name">
							</div>
							<div class="form-group">
								<label for="input-1">Price</label>
								<input type="text" name="price" onkeypress="return isNumberKeyWithFloat(event,this)"  value="<?PHP echo $package->price; ?>" class="form-control" id="price">
							</div>
							<div class="form-group">
								<label >Book</label>
								<?php 
									if(strlen($package->book_id) > 1)
									{
										$book_id  = explode(',', $package->book_id);
									}

								?>
								<select class="book form-control" name="book_id[]" multiple>
									<?php foreach ($booklist as $value) {?>
										<option <?php if(in_array($value->id,$book_id)){ echo 'selected';}?> value="<?php echo $value->id;?>"><?php echo $value->title;?></option>	
									<?php } ?>
								</select>
							</div>
							<input type="hidden" name="id" value="<?PHP echo $package->id; ?>">

							<div class="form-group">
								<label for="input-2">Package Image</label>
								<input type="file" name="package_image" class="form-control" id="input-2" onchange="readURL(this,'showImage')">
								<input type="hidden" name="packageimage" value="<?php echo $package->package_image; ?>">
								<p class="noteMsg">Note: Image Size must be lessthan 2MB.Image Height and Width less than 1000px.</p>
								<div><img id="showImage" src="<?php echo base_url().'assets/images/package/'.$package->package_image; ?>" height="100px;" width="100px;"></div>
							</div>
							<div class="form-group">
								<button type="button" onclick="updatepackage()" class="btn btn-primary shadow-primary px-5">Update</button>
							</div>
						</form>
					</div>
				</div>


			</div></div></div>
		</div>
		<script type="text/javascript">
				$(document).ready(function() {
					
			    $('.book').select2({
			    	 tags: true,
				    placeholder: "Please select book.",
			    });
			});
		</script>
		<?php
		$this->load->view('admin/comman/footerpage');
		?>
		<script type="text/javascript">
			function updatepackage(){
				$("#dvloader").show();
				var formData = new FormData($("#edit_package_form")[0]);
				$.ajax({
					type:'POST',
					url:'<?php echo base_url(); ?>admin/package/update',
					data:formData,
					cache:false,
					contentType: false,
					processData: false,
					dataType: 'json', 
					success:function(resp){
						$("#dvloader").hide();
						if(resp.status=='200'){
							document.getElementById("edit_package_form").reset();
							toastr.success(resp.message);
							window.location.replace('<?php echo base_url(); ?>admin/package');
						}else{
							var obj = resp.message;
							$.each(obj, function(i,e) {
						        toastr.error(e);
						    });
						}
					}
				});
			}
		</script>