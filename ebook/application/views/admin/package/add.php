<?php
$this->load->view('admin/comman/header');
?>

<div class="clearfix"></div>

<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add package</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
           			<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/package">Package</a></li>
					<li class="breadcrumb-item active" aria-current="page">Add package</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/package/list" class="btn btn-outline-primary waves-effect waves-light">package List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->

		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Add package</div>
						<hr>
						<form id="package_form"  enctype="multipart/form-data">
							<div class="form-group">
								<label for="input-1">package Name</label>
								<input type="text" name="package_name" class="form-control" id="package_name">
							</div>
							<div class="form-group">
								<label for="input-1">Price</label>
								<input type="text" name="price" onkeypress="return isNumberKeyWithFloat(event,this)" class="form-control" id="price">
							</div>
							<div class="form-group">
								<label >Book</label>
								<select class="book form-control" name="book_id[]" id="book_id" multiple>
									<?php foreach ($booklist as $value) {?>
										<option value="<?php echo $value->id;?>"><?php echo $value->title;?></option>	
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label for="input-2">package Image</label>
								<input type="file" name="package_image" class="form-control" id="input-2" onchange="readURL(this,'showImage')" >
								<p class="noteMsg">Note: Image Size must be lessthan 2MB.Image Height and Width less than 1000px.</p>
								<img id="showImage" src="<?php echo base_url().'assets/images/placeholder.png';?>" height="100px" width="100px"/>
							</div>
							<div class="form-group">
								<button type="button" onclick="savepackage()" class="btn btn-primary shadow-primary px-5">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div></div></div>
		</div>
		<script type="text/javascript">
				$(document).ready(function() {
					
			    $('.book').select2({
			    	 tags: true,
				    placeholder: "Please select book.",
			    });
			});
		</script>
	
</html>
		<?php
		$this->load->view('admin/comman/footerpage');
		?>

		<script type="text/javascript">
		
			function savepackage(){

				var formData = new FormData($("#package_form")[0]);
				
				$("#dvloader").show();
				$.ajax({
					type:'POST',
					url:'<?php echo base_url(); ?>admin/package/save',
					data:formData,
					cache:false,
					contentType: false,
					processData: false,
					dataType: 'json', 
					success:function(resp){
						$("#dvloader").hide();
						if(resp.status=='200'){
							document.getElementById("package_form").reset();
							toastr.success(resp.message);
							window.location.replace('<?php echo base_url(); ?>admin/package');
						}else{
							var obj = resp.message;
							$.each(obj, function(i,e) {
						        toastr.error(e);
						    });
						}
					}
				});
			}
		</script>