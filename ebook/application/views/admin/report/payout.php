<?php $this->load->view('admin/comman/header');?>
<!-- transactionList Data Show -->
<div class="clearfix"></div>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Payout Report</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Payout Report</li>
				</ol>
			</div>
		</div>
		<!-- End Breadcrumb-->

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"> Payment History</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="payout-datatable"  class="table table-bordered">
								<thead>
									<tr>
										<th> Author Name </th>
										<th> Amount </th>
										<th> Payment Method </th>
										<th> Process Date</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($payout as $value) { ?>
									<tr>
										<td> <?php echo $value->name;?></td>
										<td> <?php echo $_SESSION['currency_symbol'];?><?php echo $value->amount;?></td>
										<td> Bank Transfer</td>
										<td> <?php echo dateformate($value->created_at);?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->
	<?php $this->load->view('admin/comman/footerpage'); ?>
	
	<script type="text/javascript">
		$(document).ready(function(){  
			$('#payout-datatable').DataTable();  
		});
	</script>