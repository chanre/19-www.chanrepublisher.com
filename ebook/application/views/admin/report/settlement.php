<?php $this->load->view('admin/comman/header');?>
<div class="clearfix"></div>
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Settlement Report</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Settlement Report</li>
				</ol>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<div class="row">
			<div class="col-lg-12">
			<div class="card">
				<div class="card-header"> 
					<div class="row">
						<form>
							<div class="form-group col-xs-6">
								<label>Search : </label>
							</div>
							<div class="form-group col-xs-6">
								<select name="author_id" class="form-control col-sm-12 author_id">
									<option value=""> Select Author </option>
									<?php $author_id = isset($_REQUEST['author_id']) ? $_REQUEST['author_id'] : '';?>
									<?php foreach($author as $row){?>		
									<option value="<?php echo $row->id;?>" <?php if($row->id == $author_id){ echo 'selected';}?>>  <?php echo $row->name;?> </option>
									<?php } ?>
								</select>
							</div>							
							<?php  $start_date = date('m'); ?>
							<div class="form-group col-xs-6">
								<select name="month" class="form-control month">
									<option value=""> Select Month </option>
									<option value="1" <?php if($start_date == '1'){ echo 'selected';  }?>>January</option>
									<option value="2" <?php if($start_date == '2'){ echo 'selected';  }?>>February</option>
									<option value="3" <?php if($start_date == '3'){ echo 'selected';  }?>>March</option>
									<option value="4" <?php if($start_date == '4'){ echo 'selected';  }?>>April</option>
									<option value="5" <?php if($start_date == '5'){ echo 'selected';  }?>>May</option>
									<option value="6" <?php if($start_date == '6'){ echo 'selected';  }?>>June</option>
									<option value="7" <?php if($start_date == '7'){ echo 'selected';  }?>>July</option>
									<option value="8" <?php if($start_date == '8'){ echo 'selected';  }?>>August</option>
									<option value="9" <?php if($start_date == '9'){ echo 'selected';  }?>>September</option>
									<option value="10" <?php if($start_date == '10'){ echo 'selected';  }?>>October</option>
									<option value="11" <?php if($start_date == '11'){ echo 'selected';  }?>>November</option>
									<option value="12" <?php if($start_date == '12'){ echo 'selected';  }?>>December</option>
								</select>
							</div>
						
							<div class="form-group col-xs-6">
								<button class="btn submit"> Search </button>
							</div>
						</form>
						<div class="form-group col-xs-6 ">
							<button class="btn btn-info settlement_button"> Settlement </button>
						</div>
					</div>
				</div>

				<div class="card-body">
					<div class="table-responsive">
						<table id="transaction-datatable" class="table table-bordered">
							<thead>
								<tr>
									<th><input type="checkbox" id="selectAll" name='selectAll' class="main"></th>
									<th> Month </th>
									<th> Author Name </th>
									<th> Total Amount </th>
									<th> Author Amount </th>
									<th> Admin Commission </th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($settlement as $key => $value) { ?>
								<tr>
									<td><input type="checkbox" class="settlement" id="<?php echo $value->author_id;?>" name="settlement[]" value="<?php echo $value->author_id;?>"/> </td>
									<td> <?php echo date('M', strtotime($value->created_at));?></td>
									<td> <?php echo $value->name;?></td>
									<td> <?php echo $_SESSION['currency_symbol'];?><?php echo $value->total_amount;?></td>
									<td> <?php echo $_SESSION['currency_symbol'];?><?php echo $value->total_amount - $value->total_admin_commission_amount;?></td>
									<td> <?php echo $_SESSION['currency_symbol'];?><?php echo $value->total_admin_commission_amount;?></td>	
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- End Row-->

	<?php $this->load->view('admin/comman/footerpage'); ?>
	<script>
		$(document).ready(function(){  
		    $('#transaction-datatable').DataTable();  
			
			$(".start_date").datepicker();
		    $(".end_date").datepicker();  
		});


		$(document).ready(function () { 
			$('.settlement_button').prop('disabled', true);
	
	    	$('#selectAll').on('click', function () {
	        	if ($(this).hasClass('allChecked')) {
	            	$('input[type="checkbox"]').prop('checked', false);
					// $(".settlement_button").addClass("disabled");
					$('.settlement_button').prop('disabled', true);
	        	} else {
	            	$('input[type="checkbox"]').prop('checked', true);
	            	$('.settlement_button').prop('disabled', false);
				}
	        	$(this).toggleClass('allChecked');
	    	});
	    		
	    	$('.settlement').on('click', function () {
	    		checkboxLenght();
	    	});

	    	function checkboxLenght()
	    	{
	    		var checkebox =  $(".settlement").length;

	    		var checked =  $(".settlement:checked").length;

	    		if(checkebox == checked)
	    		{
	    			$('input[name="selectAll"]').prop('checked', true);
	    		}else{
					$('input[name="selectAll"]').prop('checked', false);
	    		}

	        	if(checked > 0){
	            	$('.settlement_button').prop('disabled', false);
	            }
	            else {
	                $('.settlement_button').prop('disabled', true);
	            }
	    	}


	    	$('.settlement_button').on('click', function () {
	        	var month = $('.month').val();

	        	var settlement = [];
	            $.each($("input[name='settlement[]']:checked"), function(){
	                settlement.push($(this).val());
	            });

				$.ajax({
					type:'POST',
					url:'<?php echo base_url();?>admin/report/settlement_update/',
					data:{"month":month,"author_id":settlement},
					dataType: "json",
					success:function(resp){
						toastr.success('Successfully settlement');    
						setTimeout(function(){
							window.location.replace('<?php echo base_url(); ?>admin/report/settlement');
						}, 1000);             
					}	
				});	
	    	});
		});

	</script>