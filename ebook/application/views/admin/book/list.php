<?php
$this->load->view('admin/comman/header');
?>
<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Books list</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/book">Books</a></li>
          <li class="breadcrumb-item active" aria-current="page">Books list</li>
        </ol>
      </div>
      <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="<?php echo base_url();?>admin/book/add" class="btn btn-outline-primary waves-effect waves-light">Add Book</a>

      </div>
    </div>
  </div>
  <!-- End Breadcrumb-->
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header"><i class="fa fa-table"></i> Books Record</div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="book-datatable" class="table table-bordered">
             <thead>
              <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Author Name</th>
                <th>view</th>
                <th>Download</th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div><!-- End Row-->

<?php $this->load->view('admin/comman/footerpage'); ?>
  <script>
	$(document).ready(function(){  
    var dataTable = $('#book-datatable').DataTable({  
      "processing":true,  
      "serverSide":true,  
      "order":[],  
      "ajax":{  
        url:"<?php echo base_url() . 'admin/book/fetch_data'; ?>",  
        type:"POST"  
      },  
      "columnDefs":[  
        {  
          //  "targets":[0, 3, 4],  
          "orderable":false,  
        },  
      ],  
    });  
  });  
  </script>