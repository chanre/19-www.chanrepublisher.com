<?php
$this->load->view('admin/comman/header');
?>



<div class="clearfix"></div>

<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Books Update</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
          			<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/book">Books</a></li>
					<li class="breadcrumb-item active" aria-current="page">update Book</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/book/list" class="btn btn-outline-primary waves-effect waves-light">Books List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<?php $vid=$booklist; ?>
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Update Book</div>
						<hr>
						<form id="edit_video_form"  enctype="multipart/form-data">
							<input type="hidden" name="id" value="<?PHP echo $vid->id; ?>">
							<div class="row col-lg-12">
	                            <div class="form-group col-lg-6">
		                        	<label for="input-1">Book Name</label>
									<input type="text" required value="<?php echo $vid->title; ?>" class="form-control" name="title" id="title" placeholder="Enter Book Name">
							    </div>
	                            <div class="form-group col-lg-6">
	                        		<label for="input-2">Book Category</label>
									<select name="category_id" required  class="form-control">
										<option value="">Select Category</option>
										<?php foreach($categorylist as $cat){ ?>
											<option value="<?php echo $cat->id; ?>" <?php if($cat->id==$vid->category_id ){ echo 'selected="selected"'; } ?> ><?php echo $cat->name?></option>
										<?php } ?>
									</select>
	                            </div>
		                    </div>
		                    <div class="row col-lg-12">
	                            <div class="form-group col-lg-6">
		                    		<label for="input-3">Book Cost</label>
									<select name="is_paid" required  class="form-control" id="is_paid">
										<option value="0" <?php if ($vid->is_paid == "0" ) echo 'selected' ; ?>>Free</option>
										<option value="1" <?php if ($vid->is_paid == "1" ) echo 'selected' ; ?>>Paid</option>
									</select>
		                    	</div>
								<div class="form-group col-lg-6">
		                    		<div id="business" >
										<label for="price">Book Price</label>
										<input type="text" required value="<?php echo $vid->price; ?>" class="form-control" name="price" id="price" placeholder="Enter Book Price">
									</div>
		                    	</div>
		                    </div>
		                    <div class="row col-lg-12">
	                            <div class="form-group col-lg-12">
	                            	<?php if($_SESSION['role_id'] == 1){?>
									<label for="input-2">Book Author</label>
									<!-- DropDown -->
									<select name="author_id" required  class="form-control">
										<option value="">Select Author</option>
										<?php $i=1;foreach($authorlist as $auth){ ?>
										<option value="<?php echo $auth->id; ?>" <?php if($auth->id==$vid->author_id ){ echo 'selected="selected"'; } ?> ><?php echo $auth->name?></option>
											<?php $i++;} ?>
									</select>
									<?php }else {?>
										<input type="hidden" name="author_id" value="<?php echo $_SESSION['id'];?>">
									<?php } ?>
	                            </div>
	                        </div>
							<div class="row col-lg-12">
	                            <div class="form-group col-lg-6">
		                            <label for="input-1"> Upload Sample Book</label>
									<input type="file" required  class="form-control" name="sample_url" id="sample_url">
									<input type="hidden" name="inputsamplebook" value="<?PHP echo $vid->sample_url; ?>">
									<div>
										<label for="input-1" value="<?PHP echo $vid->sample_url; ?>">Sample Book :- <?PHP echo $vid->sample_url; ?></label>
									</div>
	                            </div>
	                            <div class="form-group col-lg-6">
		                            <label for="input-1"> Upload Full Book</label>
									<input type="file" required  class="form-control" name="url" id="url" placeholder="select Full Book">
									<input type="hidden" name="inputfullbook" value="<?PHP echo $vid->url; ?>">
									<div>
										<label for="input-1" value="<?php echo $vid->url; ?>">Full Book :- <?PHP echo $vid->url; ?></label>
									</div>
	                            </div>
	                        </div>
							<div class="row col-lg-12">
	                            <div class="form-group col-lg-12">
		                    		<label for="input-1">Book Description</label>
									<textarea class="form-control summernote" name="description" id="description" ><?php echo $vid->description; ?></textarea>
		                    	</div>
		                    	<div class="form-group col-lg-6">
		                    		<label for="input-1"> Book Cover Poster</label>
									<input type="file" required  class="form-control" name="image" id="image"  onchange="readURL(this,'showImage')">
									<input type="hidden" name="inputbookcover" value="<?PHP echo $vid->image; ?>">
									<p class="noteMsg">Note: Image Size must be less than 2MB.Image Height and Width less than 1000px.</p>
									<img id="showImage" src="<?php echo base_url().'assets/images/book/'.$vid->image; ?>" height="100" width="100"/>
		                    	</div>
		                    	<div class="form-group col-lg-12">
									<button type="button" onclick="update_book()" class="btn btn-primary shadow-primary px-5"> Update</button>
		                    	</div>
		                    </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<?php $this->load->view('admin/comman/footerpage');?>
	<script type="text/javascript">
		$('#is_paid').on('change', function () {
			if (this.value === '1'){
				$("#business").show();
			} else {
				$("#business").hide();
			}
		});

		function update_book(){
			$("#dvloader").show();
			var formData = new FormData($("#edit_video_form")[0]);

			var textareaValue = $('#description').code();
			formData.append("description", textareaValue);

			$.ajax({
				type:'POST',
				url:'<?php echo base_url(); ?>admin/book/update',
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				dataType: "json",
				success:function(resp){
					if(resp.status=='200'){
						$("#dvloader").hide();
						document.getElementById("edit_video_form").reset();
						toastr.success(resp.message,'success');                 
						setTimeout(function(){
							window.location.replace('<?php echo base_url(); ?>admin/book');
						}, 500);
					}else{
						$("#dvloader").hide();
						var obj = resp.message;
						$.each(obj, function(i,e) {
					        toastr.error(e);
					    });
					}
				}
			});
		}
	</script>