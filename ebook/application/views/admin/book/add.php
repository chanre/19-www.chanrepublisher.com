<?php $this->load->view('admin/comman/header');?>

<div class="clearfix"></div>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Add Book</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
         			<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/book">Books</a></li>
					<li class="breadcrumb-item active" aria-current="page">Add Book</li>
				</ol>
			</div>
			<div class="col-sm-3">
				<div class="btn-group float-sm-right">
					<a href="<?php echo base_url();?>admin/booklist" class="btn btn-outline-primary waves-effect waves-light">Books List</a>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card">
					<div class="card-body">
						<div class="card-title">Add Book
							<form id="edit_video_form"  enctype="multipart/form-data">
								<div class="row col-lg-12">
		                            <div class="form-group col-lg-6">
			                        	<label for="input-1">Book Name</label>
										<input type="text" required value="" class="form-control" name="title" id="title" >
								    </div>
		                            <div class="form-group col-lg-6">
		                        		<label for="category_id">Book Category</label>
										<select name="category_id" required  class="form-control">
											<option value="">Select Category</option>
											<?php foreach($categorylist as $cat){ ?>
											<option required value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>            
											<?php } ?>
										</select>
		                            </div>
		                        </div>
		                        <div class="row col-lg-12">
		                            <div class="form-group col-lg-6">
			                        	<label for="is_paid">Book Cost</label>
										<select name="is_paid" required  class="form-control" id="purpose">
											<option value="0">Free</option>
											<option value="1">Paid</option>
										</select>
								    </div>
		                            <div class="form-group col-lg-6">
		                        		<div id="business" style="display:none">
											<label for="input-1">Book price</label>
											<input type="text" required value="" class="form-control"  name="price" id="price" placeholder="Enter Book Price">
										</div>
		                            </div>
		                        </div>
								<div class="row col-lg-12">
		                            <div class="form-group col-lg-12">
			                        	<?php if($_SESSION['role_id'] == 1){?>
											<label for="input-2">Book Author</label>
											<!-- DropDown -->
											<select name="author_id" required  class="form-control">
												<option value="">Select Author</option>
												<?php foreach($authorlist as $cat){ ?>
													<option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>            
													<?php } ?>
												</select>
										<?php }else  {?>
											<input type="hidden" name="author_id" value="<?php echo $_SESSION['id'];?>">
										<?php }?>
								    </div>
								</div>
		                            
								<div class="row col-lg-12">
		                            <div class="form-group col-lg-6">
		                            	<label for="input-1"> Upload Sample Book</label>
										<input type="file" required  class="form-control" name="sample_url" id="sample_url" placeholder="select Sample Book">
			                   		</div>
									<div class="form-group col-lg-6">
			                   			<label for="input-1"> Upload Full Book</label>
											<input type="file" required  class="form-control" name="url" id="url" placeholder="select Full Book">
			                   		</div>
								</div>
								<div class="row col-lg-12">										
									<div class="form-group col-lg-12">
										<label for="input-1">Book Description</label>
										<textarea  class="form-control summernote" name="description" id="description" ></textarea>
									</div>
									<div class="form-group col-lg-6">
		                        			<label for="input-1"> Book Cover Poster</label>
											<input type="file" required  class="form-control" name="image" id="image" onchange="readURL(this,'showImage')">
											<p class="noteMsg">Note: Image Size must be less than 2MB.Image Height and Width less than 1000px.</p>
											<img id="showImage" src="<?php echo base_url().'assets/images/placeholder.png';?>" height="100" width="100" alt="your image" />
									</div>
									<div class="form-group col-lg-12">
										<button type="button" onclick="savebook()" class="btn btn-primary shadow-primary px-5">Save</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	$this->load->view('admin/comman/footerpage');
	?>
	<script type="text/javascript">

		$('#purpose').on('change', function () {
			if (this.value === '1'){
				$("#business").show();
			} else {
				$("#business").hide();
			}
		});
		function savebook(){

			$("#dvloader").show();
			var formData = new FormData($("#edit_video_form")[0]);

			var textareaValue = $('#description').code();
			formData.append("description", textareaValue);


			$.ajax({
				type:'POST',
				url:'<?php echo base_url(); ?>admin/book/save',
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				dataType: "json",
				success:function(resp){
					$("#dvloader").hide();
					if(resp.status=='200'){
						document.getElementById("edit_video_form").reset();
						toastr.success(resp.message,'success');
						setTimeout(function(){ 
							window.location.replace('<?php echo base_url(); ?>admin/book');
						}, 500);
					}else{
						var obj = resp.message;
						$.each(obj, function(i,e) {
					        toastr.error(e);
					    });
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$("#dvloader").hide();
					toastr.error(errorThrown.msg,'failed');         
				}
			});
		}
	</script>