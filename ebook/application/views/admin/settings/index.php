<?php
  $this->load->view('admin/comman/header');
?>
<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
  <div class="row page-titles mx-0">
    <div class="col p-md-0">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/setting">Settings</a></li>
        <li class="breadcrumb-item active"><a href="javascript:void(0)">Change setting</a></li>
      </ol>
    </div>
  </div>
  <!-- row -->

  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Add Tv Show</h4>
            <div class="clearfix"></div>
            <div class="row">
              <?php $setn=array(); foreach($settinglist as $set){
                $setn[$set->key]=$set->value;
              }
              ?>  
         
              <div class="col-lg-12">
                <div>
                  <div> 
                    <ul class="nav nav-pills nav-pills-danger nav-justified top-icon" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active show" data-toggle="pill" href="#piil-17"> <span class="hidden-xs">App</span></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#piil-20"> <span class="hidden-xs">Change Password</span></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#piil-18"> <span class="hidden-xs">Admob</span></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#piil-19"> <span class="hidden-xs">Notification</span></a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" data-toggle="pill" href="#piil-21"> <span class="hidden-xs">SMTP</span></a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" data-toggle="pill" href="#piil-22"> <span class="hidden-xs">Currency</span></a>
                      </li>
                    </ul>

                    <div class="tab-content card-body">

                      <div id="piil-17" class="container tab-pane active show">
                        <form id="save_setting"  enctype="multipart/form-data">
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label for="input-1">App Name</label>
                              <input type="text" name="app_name" required class="form-control" id="input-1" placeholder="Enter Your App Name" value="<?php echo $setn['app_name'];?>">
                            </div>
                            <div class="form-group col-lg-6">
                              <label>Host Email</label>
                              <input type="text" name="host_email" required class="form-control" id="input-2" value="<?php echo $setn['host_email'];?>">
                            </div>
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label>App Version</label>
                              <input type="text" name="app_version" required class="form-control" id="app_version" value="<?php echo $setn['app_version'];?>">
                            </div>  
                            <div class="form-group col-lg-6">
                              <label> Author </label>
                              <input type="text" name="Author" required class="form-control" id="Author" value="<?php echo $setn['Author'];?>">
                            </div>
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label> email </label>
                              <input type="text" name="email" required class="form-control" id="email" value="<?php echo $setn['email'];?>">
                            </div>
                            <div class="form-group  col-lg-6">
                              <label> Contact </label>
                              <input type="text" name="contact" required class="form-control" id="contact" value="<?php echo $setn['contact'];?>">
                            </div>  
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label> Author Commission </label>
                              <input type="number" name="author_commission" required class="form-control" id="author_commission" value="<?php echo $setn['author_commission'];?>">
                            </div>  
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-12">
                              <label>App Descripation</label>
                              <textarea  name="app_desripation" required class="form-control summernote" id="app_desc" ><?php echo $setn['app_desripation'];?></textarea>
                            </div>
                            <div class="form-group  col-lg-12">
                              <label> Privacy Policy </label>
                               <textarea rows="10" cols="80" name="privacy_policy"  required class="form-control summernote" id="summernote" ><?php echo $setn['privacy_policy'];?>
                              </textarea>
                            </div>
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label for="input-2">App Image</label>
                              <input type="file" name="app_image"  onchange="readURL(this,'showImage',100,200)" required class="form-control" id="input-2" >
                              <input type="hidden" name="app_image_logo" value="<?php echo $setn['app_logo'];?>">
                              <div>
                                <p class="noteMsg">Note: Image Size must be lessthan 2MB.Image Height and Width Maximum - 100x200</p>
                                <img id="showImage" src="<?php echo base_url().'assets/images/app/'.$setn['app_logo'];?>" height="100px" width="100px"/>
                              </div>
                            </div>
                            <div class="form-group  col-lg-6">
                              <label> website </label>
                              <input type="text" name="website" required class="form-control" id="website" value="<?php echo $setn['website'];?>">
                            </div>
                          </div>
                          <div class="form-group col-lg-12">
                            <button type="button" class="btn btn-primary shadow-primary px-5" onclick="save_setting()"> Save</button>
                          </div>
                        </form>
                      </div>

                      <div id="piil-18" class="container tab-pane fade">
                         <ul class="nav  nav-tabs-danger nav-justified " role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active show" data-toggle="pill" href="#admob_1">Android</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="pill" href="#admob_2">IOS</a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <div id="admob_1" class="container tab-pane active show">
                              <form id="save_admob"  enctype="multipart/form-data">
                                <div class="row col-lg-12">
                                  <div class="form-group col-lg-12">
                                    <label for="input-1">Publisher ID</label>
                                    <input type="text" name="publisher_id" required class="form-control" id="input-1" placeholder="Enter Your App Name" value="<?php echo $setn['publisher_id'];?>">
                                  </div>
                                </div>  
                                <div class="row col-lg-12">
                                  <div class="form-group  col-lg-6">
                                    <label for="input-2">Banner Ad</label>
                                    <select name="banner_ad"  class="form-control" id="banner_ad">
                                      <option> Select Banner</option>
                                      <option value="yes" <?php if($setn['banner_ad']=='yes'){ echo 'selected="selected"'; } ?> >Yes</option>
                                      <option value="no" <?php if($setn['banner_ad']=='no'){ echo 'selected="selected"'; } ?>  >No</option>
                                    </select>
                                  </div>
                                  <div class="form-group  col-lg-6">
                                    <label for="input-2">Interstital Ad</label>
                                    <select name="interstital_ad"  class="form-control" id="interstital_ad">
                                      <option> Select Banner</option>
                                      <option value="yes" <?php if($setn['interstital_ad']=='yes'){ echo 'selected="selected"'; } ?> >Yes</option>
                                      <option value="no" <?php if($setn['interstital_ad']=='no'){ echo 'selected="selected"'; } ?>  >No</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="row col-lg-12">
                                  <div class="form-group col-lg-6">
                                    <label for="input-1">Banner Ad ID</label>
                                    <input type="text" name="banner_adid" required class="form-control" id="input-1" placeholder="Enter Your App Name" value="<?php echo $setn['banner_adid'];?>">
                                  </div>
                                  <div class="form-group col-lg-6">
                                    <label for="input-1">Interstital Ad ID</label>
                                    <input type="text" name="interstital_adid" required class="form-control" id="input-1" placeholder="Enter Your App Name" value="<?php echo $setn['interstital_adid'];?>">
                                  </div>
                                </div>
                                <div class="form-group col-lg-12">
                                  <button type="button" class="btn btn-primary shadow-primary px-5" onclick="save_admob()"> Save</button>
                                </div>
                              </form>
                            </div>
                            <div id="admob_2" class="container tab-pane fade"> Coming Soon
                            </div>
                          </div>
                      </div>
                      
                      <div id="piil-20" class="container tab-pane fade">
                        <form id="save_password" >
                          <div class="form-group">
                            <label for="input-1">New Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Enter new password">
                          </div>
                          <div class="form-group">
                            <label for="input-2">Confirm Password</label>
                            <input type="password" name="confirm_password" class="form-control" id="confirm_password" value="" placeholder="Enter confirm password">
                          </div>
                          <input type="hidden" name="id" value="<?php echo $_SESSION['id'];?>">
                          
                          <div class="form-group">
                            <button type="button" class="btn btn-primary shadow-primary px-5" onclick="change_password()"> Save</button>
                          </div>
                        </form>
                      </div>

                      <div id="piil-19" class="container tab-pane fade">
                        <form id="save_signal_noti"  enctype="multipart/form-data">
                          <div class="form-group">
                            <label for="input-1">OneSignal App ID</label>
                            <input type="text" name="onesignal_apid" required class="form-control" id="input-1" placeholder="Enter Your App Name" value="<?php echo $setn['onesignal_apid'];?>">
                          </div>
                          <div class="form-group">
                            <label for="input-2">OneSignal Rest Key</label>
                            <input type="text" name="onesignal_rest_key" required class="form-control" id="input-2" value="<?php echo $setn['onesignal_rest_key'];?>">
                          </div>
                          <div class="form-group">
                            <button type="button" class="btn btn-primary shadow-primary px-5" onclick="save_signal_noti()"> Save</button>
                          </div>
                        </form>
                      </div>

                      <div id="piil-21" class="container tab-pane fade">
                        <form id="save_smtp_setting"  enctype="multipart/form-data">
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label for="input-2">Host</label>
                              <input type="text" name="host" required class="form-control" id="host" value="<?php echo $smtp_setting->host;?>">
                            </div>
                             <div class="form-group col-lg-6">
                              <label for="input-2">Port</label>
                              <input type="text" name="port" required class="form-control" id="port" value="<?php echo $smtp_setting->port;?>">
                            </div>
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label for="input-2">User name</label>
                              <input type="text" name="user" required class="form-control" id="user" value="<?php echo $smtp_setting->user;?>">
                            </div>
                             <div class="form-group col-lg-6">
                              <label for="input-2">Password</label>
                              <input type="text" name="pass" required class="form-control" id="pass" value="<?php echo $smtp_setting->pass;?>">
                            </div>
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label for="input-2">From name</label>
                              <input type="text" name="from_name" required class="form-control" id="from_name" value="<?php echo $smtp_setting->from_name;?>">
                            </div>
                            <div class="form-group col-lg-6">
                              <label for="input-2">From Email</label>
                              <input type="text" name="from_email" required class="form-control" id="from_email" value="<?php echo $smtp_setting->from_email;?>">
                            </div>
                          </div>
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label for="input-1">Protocol</label>
                              <input type="text" name="protocol" required class="form-control" id="protocol" placeholder="Enter Your protocol" value="<?php echo $smtp_setting->protocol;?>">
                            </div>
                          </div>
                          <div class="form-group col-lg-12">
                            <button type="button" class="btn btn-primary shadow-primary px-5" onclick="save_smtp_setting()"> Save</button>
                          </div>
                        </form>
                      </div>

                       <div id="piil-22" class="container tab-pane fade">
                        <form id="save_currency_setting"  enctype="multipart/form-data">
                          <div class="row col-lg-12">
                            <div class="form-group col-lg-6">
                              <label for="input-2">Set Currency</label>
                              <select class="form-control" name="currency">
                                <option> Select Currency</option>
                                <?php foreach ($currency as $key => $value) {?>
                                <option value="<?php echo $value->id;?>" <?php if($value->status == 1){echo "selected";}?>> (<?php echo $value->symbol;?>)<?php echo $value->name;?></option>
                                <?php } ?>
                              </select>
                            </div>
                             
                          </div>
                          <div class="form-group col-lg-12">
                            <button type="button" class="btn btn-primary shadow-primary px-5" onclick="save_currency_setting()"> Save</button>
                          </div>
                        </form>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function save_setting(){

    var formData = new FormData($("#save_setting")[0]);

    var app_desc = $('#app_desc').code();
    formData.append("app_desripation", app_desc);

    var textareaValue = $('#summernote').code();
    formData.append("privacy_policy", textareaValue);


    displayLoader();
    $.ajax({
      type:'POST',
      url:'<?php echo base_url(); ?>admin/setting/save' ,
      data:formData,
      cache:false,
      contentType: false,
      processData: false,
      dataType: "json",
      success:function(resp){
        hideLoader();
        if(resp.status=='200'){
          toastr.success(resp.message,'Setting saved.');
          setTimeout(function(){ 
            window.location.replace('<?php echo base_url(); ?>admin/setting'); 
          }, 500);
        }else{
          toastr.error(resp.message);
        }
      }
    });
  }


  function change_password(){
    var formData = new FormData($("#save_password")[0]);
    var password = jQuery('#password').val();
    var confirm_password = jQuery('#confirm_password').val();
    if(password!=confirm_password){
      toastr.error('Password and confirm password not match.');
      return false;
    }
    displayLoader();
    $.ajax({
      type:'POST',
      url:'<?php echo base_url(); ?>admin/setting/change_password' ,
      data:formData,
      cache:false,
      contentType: false,
      processData: false,
      dataType: "json",
      success:function(resp){
        hideLoader();
        if(resp.status=='200'){
          toastr.success(resp.message,'Password change sucessfully.');
          setTimeout(function(){ 
            window.location.replace('<?php echo base_url(); ?>admin/setting'); 
          }, 500);
        }else{
          toastr.error(resp.message);
        }
      }
    });
  }



  function save_currency_setting(){
    var formData = new FormData($("#save_currency_setting")[0]);
  
    displayLoader();
    $.ajax({
      type:'POST',
      url:'<?php echo base_url(); ?>admin/setting/save_currency' ,
      data:formData,
      cache:false,
      contentType: false,
      processData: false,
      dataType: "json",
      success:function(resp){
        hideLoader();
        if(resp.status=='200'){
          toastr.success(resp.message,'Update currency setting sucessfully.');
          setTimeout(function(){ 
            window.location.replace('<?php echo base_url(); ?>admin/setting'); 
          }, 500);
        }else{
          toastr.error(resp.message);
        }
      }
    });
  }
  function save_admob(){
    var formData = new FormData($("#save_admob")[0]);
    displayLoader();
    $.ajax({
      type:'POST',
      url:'<?php echo base_url(); ?>admin/setting/save' ,
      data:formData,
      cache:false,
      contentType: false,
      processData: false,
      dataType: "json",
      success:function(resp){
        hideLoader();
        if(resp.status=='200'){
          toastr.success(resp.message,'Password change sucessfully.');
          setTimeout(function(){ 
            window.location.replace('<?php echo base_url(); ?>admin/setting'); 
          }, 500);
        }else{
          toastr.error(resp.message);
        }
      }
    });
  }

  function save_signal_noti(){
    var formData = new FormData($("#save_signal_noti")[0]);
    displayLoader();
    $.ajax({
      type:'POST',
      url:'<?php echo base_url(); ?>admin/setting/save' ,
      data:formData,
      cache:false,
      contentType: false,
      processData: false,
      dataType: "json",
      success:function(resp){
        hideLoader();
        if(resp.status=='200'){
          toastr.success(resp.message,'Password change sucessfully.');
          setTimeout(function(){ location.reload(); }, 500);
        }else{
          toastr.error(resp.message);
        }
      }
    });
  }


  function save_smtp_setting(){
    var formData = new FormData($("#save_smtp_setting")[0]);
     displayLoader();
    $.ajax({
      type:'POST',
      url:'<?php echo base_url(); ?>admin/setting/save_smtp_setting',
      data:formData,
      cache:false,
      contentType: false,
      processData: false,
      success:function(resp){
        hideLoader();
        var obj = JSON.parse(resp);
        if(obj.status == '200')
        {
            toastr.success(obj.msg);
            setTimeout(function () {
            window.location.replace('<?php echo base_url(); ?>admin/setting');
            }, 500);
        }else
        {
          toastr.error(obj.msg);
        }
      }
    });
  }
</script>

<?php $this->load->view('admin/comman/footerpage'); ?>