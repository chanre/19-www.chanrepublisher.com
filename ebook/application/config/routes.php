<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
| my-controller/my-method	-> my_controller/my_method
*/

$route['api/login'] = 'api/user/login';
$route['api/registration'] = 'api/user/registration';
$route['api/profile'] = 'api/user/profile';
$route['api/update_profile'] = 'api/user/update_profile';
$route['api/categorylist'] = 'api/home/categorylist';
$route['api/book_by_category'] = 'api/home/book_by_category';
$route['api/autherlist'] = 'api/home/autherlist';
$route['api/book_by_author'] = 'api/home/book_by_author';
$route['api/readcount_by_author'] = 'api/home/readcount_by_author';
$route['api/booklist'] = 'api/home/booklist';
$route['api/bookdetails'] = 'api/home/bookdetails';
$route['api/popularbooklist'] = 'api/home/popularbooklist';
$route['api/free_paid_booklist'] = 'api/home/free_paid_booklist';
$route['api/related_item'] = 'api/home/related_item';
$route['api/newarriaval'] = 'api/home/newarriaval';
$route['api/feature_item'] = 'api/home/feature_item';
$route['api/add_view'] = 'api/home/add_view';
$route['api/add_download'] = 'api/home/add_download';
$route['api/alldownload'] = 'api/home/alldownload';
$route['api/add_comment'] = 'api/home/add_comment';
$route['api/view_comment'] = 'api/home/view_comment';
$route['api/check_coupon_code'] = 'api/home/check_coupon_code';
$route['api/add_transaction'] = 'api/home/add_transaction';
$route['api/purchaselist'] = 'api/home/purchaselist';
$route['api/add_continue_read'] = 'api/home/add_continue_read';
$route['api/continue_read'] = 'api/home/continue_read';
$route['api/add_bookmark'] = 'api/home/add_bookmark';
$route['api/checkbookmark'] = 'api/home/checkbookmark';
$route['api/all_bookmark'] = 'api/home/all_bookmark';
$route['api/general_setting'] = 'api/home/general_setting';
$route['api/userlist'] = 'api/home/userlist';
$route['api/add_rating'] = 'api/home/add_rating';
$route['api/get_package'] = 'api/home/get_package';
$route['api/get_ads_banner'] = 'api/home/get_ads_banner';


$route['admin'] = 'admin/login';
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;