<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class CRUD_model extends CI_Model
{
    
  public function __construct()
  {
    $this->load->database();
  }

  public function p($data)
  {
    echo '<pre>';
    print_r($data);exit;
  }
  
  public function getAdminAuthorEarning()
  {
    $this->db->select("sum(author_commission_amount) as total_commission, sum(amount) as total_earning");
    $this->db->from("transaction");
    $this->db->where('settle','0');
    $q = $this->db->get();
    return $q->row();
  }

  public function getAuthorEarning($id)
  {
    $this->db->select("sum(author_commission_amount) as total_commission, sum(amount) as total_earning");
    $this->db->from("transaction");
    $this->db->where('author_id',$id);
    $this->db->where('settle','0');
    $q = $this->db->get();
    return $q->row();
  }

  public function update_currency()
  {
    $insert['status'] = 0;
    $this->db->update('currency', $insert);
  }


  public function getSalesReport($id,$start_date,$end_date)
  {
    $this->db->select("transaction.*,user.fullname,book.title,author.name");
    $this->db->from("transaction");
    $this->db->join('user', "transaction.user_id=user.id");
    $this->db->join('book', "transaction.book_id=book.id");
    $this->db->join('author', "transaction.author_id=author.id");
    if($id)
    {
      $this->db->where('transaction.author_id',$id);
    }
    $this->db->where('settle','0');
    $this->db->where('transaction.created_at >=', $start_date);
    $this->db->where('transaction.created_at <=', $end_date);
    $q = $this->db->get();
    return $q->result();
  }
 

  public function getSalesReportMonth($id,$start_date,$end_date)
  {
    $this->db->select("transaction.*,author.name,sum(author_commission_amount) as total_admin_commission_amount, sum(amount) as total_amount");
    $this->db->from("transaction");
    $this->db->join('author', "transaction.author_id=author.id");
    if($id)
    {
      $this->db->where('transaction.author_id',$id);
    }
    $this->db->group_by("transaction.author_id");
    $this->db->where('settle','0');
    $this->db->where('transaction.created_at >=', $start_date);
    $this->db->where('transaction.created_at <=', $end_date);
    $q = $this->db->get();
    return $q->result();
  }


  public function view_website($where){
    $this->db->set('views', 'views+1', FALSE);
    $this->db->where($where);
    $this->db->update('website_analysis');
  }
   
  public function get_book_id($id)
  {
    $this->db->select("*");
    $this->db->from("book");
    $this->db->where_in('id',$id);
    $q = $this->db->get();
    return $q->result();
  }
  
  // Get count 
  public function getCount($where,$tablename)
  {
    $this->db->select('*');
    $this->db->from($tablename);
    if(!empty($where)){
      $this->db->where($where);
    }
    $query = $this->db->get();
    $row = $query->result();
    return $row;
  }

  public function updateByIdWithcount($id,$field_name,$value,$tablename){
    $this->db->set($field_name, $field_name.'+'.$value, FALSE);
    $this->db->where('id', $id);
    $this->db->update($tablename);
    return $id; 
  } 

  public function get($where,$tablename,$field_name=null,$order_by='desc')
  {
    $this->db->select('*');
    $this->db->from($tablename);
    if(!empty($where)){
      $this->db->where($where);
    }
    if($field_name){
      $this->db->order_by($field_name, $order_by);
    }
    $query = $this->db->get();
    $row = $query->result();
    return $row;
  }

	public function getById($where,$tablename)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		if(!empty($where)){
			$this->db->where($where);
		}
    $query = $this->db->get();
		$row = $query->row();
		return $row;
	}

  public function get_join_allrecord($table1, $table2, $joinid, $field, $where = Null) {
    $this->db->select($field);
    $this->db->from($table1);
    $this->db->join($table2, $joinid);
    $query = $this->db->get();
    $row = $query->result();
    return $row;
  }

  public function get_two_join_allrecord($table1, $table2, $table3, $joinid1, $joinid2, $field) {
    $this->db->select($field);
    $this->db->from($table1);
    $this->db->join($table2, $joinid1);
    $this->db->join($table3, $joinid2);
    $query = $this->db->get();
    $row = $query->result();
    return $row;
  }

  public function getLastRecord($where,$tablename)
  {
    $this->db->select('*');
    $this->db->from($tablename);
    $this->db->where($where);
    $this->db->limit(1);
    $this->db->order_by('id',"desc");
    $query = $this->db->get();
    $row = $query->row();
    return $row;
  }

  public function insert($data,$tablename){
    $data = $this->security->xss_clean($data);
    $insert = [];
    foreach ($data as  $key =>$value) {
      $insert[$key] = $this->db->escape_str($value);
    }

      $query = $this->db->insert($tablename,$data);
      return $this->db->insert_id();
  }

  public function update($id,$field_name,$data,$tablename){
 
    $data = $this->security->xss_clean($data);
    $insert = [];
    foreach ($data as  $key =>$value) {
      $insert[$key] = $this->db->escape_str($value);
    }
    
    $this->db->where($field_name, $id);
    $this->db->update($tablename, $insert);
    return $id; 
  }

  public function  delete($where,$tablename)
  {
    $this->db->where($where);
    $this->db->delete($tablename);
    return true;
  }

  public function delete_all($id,$field_name,$tablename)
  {
    $this->db->where($field_name, $id);
    $this->db->delete($tablename);
    return true; 
  }

  public function imageupload($imageName,$imgname, $uploadpath,$widths=2500,$heights=2500){

    if (!file_exists($uploadpath)) {
        mkdir($uploadpath, 0777, true);
    }
    
    if(empty($imageName['name'])){
      $res=array('status'=>'400','message'=>'Please Upload Image first.');
      echo json_encode($res);exit;
    }
    if(!empty($imageName['name']) && ($imageName['error']==1 || $imageName['size']>3215000)){
      $res=array('status'=>'202','message'=>'Max 2MB file is allowed for image.');
      echo json_encode($res);exit;
    }else{
      list($width, $height) = getimagesize($imageName['tmp_name']);
      if($width>$widths || $height >$heights){
        $res=array('status'=>'201','message'=>'Image height and width must be less than height .'.$heights.' and width '.$widths);
        echo json_encode($res);exit;
      }else{

        $catImg = $imageName['name'];
        $ext = pathinfo($catImg);
        $catImages = str_replace(array(' ', '.', '-', '`'), '_', $ext['filename']);

        $imgname_s = $imgname;
        if($imgname == 'url')
        {
          $imgname_s  = 'book';
        }
        $category_image = $imgname_s.'_'.time().'.'.$ext['extension'];       
        $config = array(
          'allowed_types' => '*',
          'upload_path' => $uploadpath,
          'file_name' => $category_image
        );
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload($imgname);
        return $category_image;
      }

    }
  }

  public function videoupload($imageName,$imgname, $uploadpath){

      if(empty($imageName['name'])){
        $res=array('status'=>'400','message'=>'Please Upload Image first.');
        echo json_encode($res);exit;
      } 

      $catImg = $imageName['name'];
      $ext = pathinfo($catImg);
      $catImages = str_replace(array(' ', '.', '-', '`'), '_', $ext['filename']);
      $category_image =time().'.'.$ext['extension'];       
      $config = array(
        'allowed_types' => '*',
        'upload_path' => $uploadpath,
        'file_name' => $category_image
      );
      $this->load->library('upload');
      $this->upload->initialize($config);
      $this->upload->do_upload($imgname);
      return $category_image;
  }

  function make_query($table,$select_column,$order_column,$searcharray=null,$where=null)  
  {  
      $this->db->select($select_column);  
      $this->db->from($table);

      if(isset($_POST["search"]["value"]))  
      {
        if($searcharray)
        {  
          foreach ($searcharray as $key => $value) {
            $this->db->or_like($value, $_POST["search"]["value"]);  
          }
        }
      }
    
      if($where != '')
      {  
        $this->db->where($where);
      }
     

      if(isset($_POST["order"]))  
      {  
        $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
      }  
      else  
      {  
        $this->db->order_by('id', 'DESC');  
      }  
  }  

  function make_datatables($table,$select_column,$order_column,$search=null,$where=null){  

      $this->make_query($table,$select_column,$order_column,$search=null,$where);  



      if($_POST["length"] != -1)  
      {  
          $this->db->limit($_POST['length'], $_POST['start']);  
      }  
      $query = $this->db->get();  
      return $query->result();  
  }
     
  function get_filtered_data($table,$select_column,$order_column,$search=null,$where=null){  
      $this->make_query($table,$select_column,$order_column,$search=null,$where);  
      $query = $this->db->get();  
      return $query->num_rows();  
  } 

  function get_all_data($table)  
  {   
      $this->db->select("*");  
      $this->db->from($table);  
      return $this->db->count_all_results();  
  }
}
?>
