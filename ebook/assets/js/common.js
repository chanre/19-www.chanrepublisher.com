function displayLoader(){
    $("#dvloader").css('display','block');
    $("body").addClass('overlay');
}

function hideLoader(){
    $("#dvloader").css('display','none');
    $("body").removeClass('overlay');
}

function readURL(input,id){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#'+id).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function removefile(imageFile){
    imageFile.val(''); 
}


function isNumberKeyWithFloat(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
        return false;
    else {
        var len = $(element).val().length;
        var index = $(element).val().indexOf('.');
        if (index > 0 && charCode == 46) {
            return false;
        }   
        if (index > 0) {
            var CharAfterdot = (len) - index;
            if (CharAfterdot > 2 ) {
                return false;
            }
        }
    }
    return true;
}

function isNumberKeyWithInt(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
    {
        return false;
    }
    else
    {
        if(charCode == 46)
        { 
            return false;
        }
        return true;
    }
}


function delete_record(id,tablename){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this "+tablename+"!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type:'POST',
                url:base_url+'admin/common/delete_record',
                data:{tablename:tablename,id:id},
                success:function(resp){
                swal("Deleted!", "Your "+tablename+"  has been deleted.", "success");
                    setTimeout(function(){ location.reload(); }, 1500);
                }
            });
         } else {
            swal("Cancelled", "Your "+tablename+" is safe :)", "error");
        }
    });
}

$(document).ready(function() {
    $('#default-datatable').DataTable();
});

function status_change(id,tablename){
    var status = $('#status_change_'+tablename+id).val();
    if(status=='enable'){
        var status_a='disable';
    }else{
        var status_a='enable';
    } 
    swal({
        title: "Are you sure?",
        text: "You want to "+status_a+" "+tablename+"!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes,"+ status_a +" it!",
        cancelButtonText: "No!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type:'POST',
                url:base_url+'admin/status_change',
                data:{tablename:tablename,id:id,status:status},
                success:function(resp){
                    swal( "Your "+tablename+"  has been "+ status_a+ ".", "success");
                    jQuery('#'+tablename+id).html(status_a); 
                    jQuery('#status_change_'+tablename+id).val(status_a); 
                }
            });
        } else {
            swal("Cancelled", "Your "+tablename+" is safe :)", "error");
        }
    });
  }


$('.summernote').summernote({
      height: 150
    });